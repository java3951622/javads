/* 0
 * 1	1
 * 2	3	5
 * 8	13	21	34
 */

import java.io.*;
class Demo{
	public static void main(String[] arg)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the number of rows:");
		int r = Integer.parseInt(br.readLine());
		int a=0,b=1,sum=0;
		for(int i=1; i<=r; i++){
			for(int j=1; j<=i; j++){
				System.out.print(sum+"\t");
				a = b;
				b = sum;
				sum = a+b;
			}
			System.out.println();
		}
	}
}
