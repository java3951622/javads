/* #	=	=	=
 * =	#	=	=
 * =	=	#	=
 * =	=	=	#
 */

import java.io.*;
class demo{
	public static void main(String[] arg)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Entr the row:");
		int r=Integer.parseInt(br.readLine());
		for(int i=1; i<=r; i++){
			for(int j=1; j<=r; j++){
				if(i==j){
					System.out.print("#"+"\t");
				}else{
					System.out.print("="+"\t");
				}
			}
			System.out.println();
		}
	}
}
