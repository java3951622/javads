/* WAP to take a number as input & print addition of factorials of each digit from that number.
 * I/P-> 1234
 * O/P-> 33
 */
 import java.io.*;
 class fact{
	 public static void main(String[] arg)throws IOException{
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
System.out.println("Enter the number:");
int num = Integer.parseInt(br.readLine());
	    int sum = addition(num);
	    System.out.println("Addition of fctorials= "+sum);

	 }
       static int addition(int num){
		 int sum=0;
		 while(num!=0){
			 int r = num%10;
			 int mult = 1;
                       for(int i=1; i<=r; i++){
                         mult = mult*i;
		       }
		       sum = sum+mult;
		       num = num/10;
		 }
		 return sum;
	 }
 }
