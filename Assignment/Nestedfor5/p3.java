/* 5	4	3	2	1
 * 8	6	4	2
 * 9	6	3
 * 8	4
 * 5
 */

import java.io.*;
class demo{
	public static void main(String[] arg)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the number of rows:");
		int r=Integer.parseInt(br.readLine());
		int n=1;
		for(int i=1; i<=r; i++){
			int m = r-i+1;
			for(int j=1; j<=r-i+1; j++){
				int x = n*m;
				System.out.print(x + "\t");
				m--;
			}
			n++;
			System.out.println();
		}
	}
}
