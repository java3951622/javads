/* WAP tp print all even numbers in reverse order & odd numbers in standard way.Both seperately. Within range.Take start & end from user.
 * I/P-> start=2
 *       end=9
 * O/P-> 8 6 4 2
 *       3 5 7 9
 */

import java.io.*;
class num{
	public static void main(String[] arg)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Entr start:");
		int s=Integer.parseInt(br.readLine());
		System.out.println("Entr end:");
		int e=Integer.parseInt(br.readLine());

		for(int i=e; i>=s; i--){
			if(i%2 == 0){
				System.out.print(i+"\t");
			}
		}
		System.out.println();
		for(int i=s; i<=e; i++){
			if(i%2 == 1){
				System.out.print(i+"\t");
			}
		}
		System.out.println();
	}
}
