/* WAp to print sum of n natural numbers
 */


import java.util.*;
class sum{
        static int sum(int n){
                if(n==1)
                        return 1;
                
                return n+sum(n-1);
        }
        public static void main(String[] arg){
                Scanner sc = new Scanner(System.in);
                System.out.println("Enter number:");
                int n = sc.nextInt();
                System.out.println("Sum :"+sum(n));
        }
}
