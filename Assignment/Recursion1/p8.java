/* WAP to count occurence of specific digit in given number
 */


import java.util.*;
class occur{
	static int c = 0;
        static void num(int n,int k){
                if(n ==0)
                        return;
                if(n%10 == k){
			c++;
		}

                num(n/10,k);
        }
        public static void main(String[] arg){
                Scanner sc = new Scanner(System.in);
                System.out.println("Enter number:");
                int n = sc.nextInt();
		System.out.println("Enter digit to count:");
		int k = sc.nextInt();
                num(n,k);
		System.out.println("Count of digit is :"+c);
        }
}
