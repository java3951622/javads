/* WAP to calculate sum of digits of given positive integers
 */


import java.util.*;
class DSum{
        static int sum(int n){
                if(n==0)
                        return 0;
                
                return n%10+sum(n/10);
        }
        public static void main(String[] arg){
                Scanner sc = new Scanner(System.in);
                System.out.println("Enter number:");
                int n = sc.nextInt();
                System.out.println("Sum of digits is:"+sum(n));
        }
}
