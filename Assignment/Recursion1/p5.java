/* WAp to check number is prime or not
 */


import java.util.*;
class prime{
	static int c = 0;
        static void prime(int n,int i){
                if(i>n)
                        return;
                if(n%i == 0){
			c++;
		}
                prime(n,i+1);
        }
        public static void main(String[] arg){
                Scanner sc = new Scanner(System.in);
                System.out.println("Enter number:");
                int n = sc.nextInt();
                prime(n,1);
		if(c == 2)
			System.out.println("Number is prime");
		else
			System.out.println("Number is not prime");
        }
}
