/* WAP to find factorial of number 
 */

import java.util.*;
class fact{
        static int fact(int n){
                if(n==1)
                        return 1;
                return n*fact(n-1);

         }
        public static void main(String[] arg){
                Scanner sc = new Scanner(System.in);
                System.out.println("Enter number:");
                int n = sc.nextInt();
                System.out.println("Factorial of number is :"+fact(n));
        }
}
