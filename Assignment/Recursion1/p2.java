/* WAP to display first 10 natural numbers in reverse order
 */


import java.util.*;
class print{
        static void num(int n){
                if(n>10)
                        return;
                System.out.println(n);
                num(n+1);
        }
        public static void main(String[] arg){
                Scanner sc = new Scanner(System.in);
           //     System.out.println("Enter number:");
             //   int n = sc.nextInt();
                num(1);
        }
}
