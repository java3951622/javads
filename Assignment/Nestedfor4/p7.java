/* F
 * E 1
 * D 2 E
 * C 3 D 4
 * B 5 C 6 D
 * A 7 B 8 C 9
 */

class seven{
	public static void main(String[] arg){
		char ch1='F';
		int n=1,r=6;
		for(int i=1; i<=r; i++){
			char ch2=ch1;
			for(int j=1; j<=i; j++){
				if(j%2==0){
					System.out.print(n +"\t");
					n++;
				}else{
					System.out.print(ch2 +"\t");
					ch2++;
				}
			}
			System.out.println();
			ch1--;
		}
	}
}

