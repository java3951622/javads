/* 10
 * I	H
 * 7	6	5
 * D	C	B	A
 */

class eight{
	public static void main(String[] arg){
		int r=4;
		int n=(r*(r+1))/2;
		char ch='J';
		for(int i=1; i<=r; i++){
			for(int j=1; j<=i; j++){
				if(i%2==1){
					System.out.print(n +"\t");
				}else{
					System.out.print(ch +"\t");
				}
				n--;
				ch--;
			}
			System.out.println();
		}
	}
}
