/* A 	B	C	D
 * B	C	D
 * C	D
 * D
 */

class five{
	public static void main(String[] arg){
		char ch1='A';
		int r=4;
		for(int i=1; i<=r; i++){
			char ch2=ch1;
			for(int j=1; j<=r-i+1; j++){
				System.out.print(ch2 +"\t");
				ch2++;
			}
			System.out.println();
			ch1++;
		}
	}
}
