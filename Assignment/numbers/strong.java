//Strong number: sum of digits factorials=number.
//145=1!+4!+5!

class strong{
	public static void main(String[] ar){
		int n=156;
		int temp=n;
		int sum=0;
		while(n!=0){
			int r=n%10;
			int m=1;
			for(int i=1; i<=r; i++){
				m=m*i;
			}
			sum=sum+m;
			n=n/10;
		}
		if(sum==temp){
			System.out.println("Strong number");
		}else{
			System.out.println("Not strong number");
		}
	}
}
