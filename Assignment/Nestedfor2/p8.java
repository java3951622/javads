/* A b C d
 * E f G h
 * I j K l
 * M n O p
 */

class alpha{
	public static void main(String ar[]){
		char ch1='A';
		char ch2='a';
		int r=4;
		for(int i=1; i<=r; i++){
			for(int j=1; j<=r; j++){
				if(j%2==1){
					System.out.print(ch1+" ");
				}else{
					System.out.print(ch2+" ");
				}
				ch1++;
				ch2++;
			}
			System.out.println();
		}
	}
}

