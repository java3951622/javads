/* 26 Z 25 Y
 * 24 X 23 W 
 * 22 V 21 U
 * 20 T 19 S
 */

class five{
	public static void main(String[] arg){
		int r=4,n=26;
		char ch='Z';
		for(int i=1; i<=r; i++){
			for(int j=1; j<=r; j++){
				if(j%2==1){
					System.out.print(n +"\t");
					n--;
				}else{
					System.out.print(ch +"\t");
					ch--;
				}
			}
			System.out.println();
		}
	}
}
