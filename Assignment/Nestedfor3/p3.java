/* 10
 * 9  8 
 * 7  6  5
 * 4  3  2  1
 */

class three{
	public static void main(String[] arg){
		int r=4;
		int n=(r*(r+1))/2;
		for(int i=1; i<=r; i++){
			for(int j=1; j<=i; j++){
				System.out.print(n +"\t");
				n--;
			}
			System.out.println();
		}
	}
}
