/* 1
 * 8  27
 * 64 125 216
 */

class nine{
	public static void main(String[] arg){
		int n=1;
		int r=4;
		for(int i=1; i<=r; i++){
			for(int j=1; j<=i; j++){
				System.out.print((n*n*n) +"\t");
				n++;
			}
			System.out.println();
		}
	}
}
