/* WAP take 7 characters as an input, print only vowels from the array
 * Input-> a b c o d p e
 * Output-> a o e
 */

import java.io.*;
class vowels{
	public static void main(String[] arg)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size :");
	       int size = Integer.parseInt(br.readLine());
                char [] arr = new char[size];
                System.out.println("Enter elements:");
                for(int i=0; i<arr.length; i++){
	          arr[i] = (char)br.read();
		  br.skip(1);
		}
		System.out.println("Vowels are:");
		for(int i=0; i<arr.length; i++){
                   if(arr[i]=='a' || arr[i]=='e' || arr[i]=='i' || arr[i]=='o'||arr[i]=='u'|| arr[i]=='A'|| arr[i]=='E'|| arr[i]=='O'|| arr[i]=='I'|| arr[i]=='U'){
	 System.out.println(arr[i]);
		   }
		}
	}
}	
