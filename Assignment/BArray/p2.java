/* WAP to take size of array from user & also take elements from user.Print product of even elements only.
 */
 import java.io.*;
 class even{
	 public static void main(String[] arg)throws IOException{
		 BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		 System.out.println("Enter size:");
		 int size = Integer.parseInt(br.readLine());
		 int [] arr = new int[size];
		 int mult=1;
		 System.out.println("Enter the elements:");
		 for(int i=0; i<arr.length; i++){
			 arr[i] = Integer.parseInt(br.readLine());
			 if(arr[i]%2==0){
				 mult = mult*arr[i];
			 }
		 }
		 System.out.println("Product of odd Numbers = "+mult);
	 }
 }
