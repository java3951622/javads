/*  WAP to take size of array from user & also take integer elements from user. Print sum of odd elements./
 */

import java.io.*;
class odd{
	public static void main(String[] arg)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the size of array:");
		int size = Integer.parseInt(br.readLine());
		int[] arr = new int[size];
		int sum=0;
		System.out.println("ENter the elments of array:");
		for(int i=0; i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
			if(arr[i]%2 == 1){
				sum = sum + arr[i];
			}
		}
		System.out.println("Sum of odd numbers = "+sum);
	}
}


