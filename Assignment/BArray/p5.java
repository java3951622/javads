/* WAP to take elemnts from user & print only that are divisible by 5.
 */

import java.io.*;
class five{
	public static void main(String[] ard)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the size:");
		int s = Integer.parseInt(br.readLine());
		int [] arr = new int [s];
		System.out.println("Enter array elements:");
		for(int i=0; i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		 System.out.println("Numbers divisible by 5 are:");
		for(int i=0; i<arr.length; i++){
			if(arr[i]%5 == 0){
				System.out.print(arr[i]+"\t");
			}
		}
		 System.out.println();
	}
}

