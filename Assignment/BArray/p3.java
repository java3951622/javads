/* WAP to take elements from user.Print product of odd index only.
 */

import java.io.*;
class odd{
	public static void main(String[] arg)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the size of array:");
		int size = Integer.parseInt(br.readLine());
		int [] arr = new int[size];
		int mult=1;
		System.out.println("Enter elemnets of array:");
		for(int i=0; i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
			if(i%2==1){
				mult = mult*arr[i];
			}
		}
		System.out.println("Product of odd index = "+mult);
	}
}

