/* WAP to find sum of even & odd numbers.
 */

import java.io.*;
class sum{
	public static void main(String[] arg)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the size:");
		int size = Integer.parseInt(br.readLine());
		int [] arr = new int[size];
		int sum1=0,sum2=0;
		System.out.println("Enter the elements of array:");
		for(int i=0; i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
			if(arr[i]%2==0){
				sum1 = sum1+arr[i];
			}else{
				sum2 = sum2+arr[i];
			}
		}
		System.out.println("Sum of even numbers = "+sum1);
		System.out.println("Sum of odd numbers = "+sum2);
	}
}
