/* WAP to create array take elements from user.Find the sum of all elements in the array
 */

import java.io.*;
class sum{
	public static void main(String[] arg)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the size:");
		int s = Integer.parseInt(br.readLine());
		int [] arr = new int [s];
		int sum = 0;
		System.out.println("Enter the elements:");
		for(int i=0; i<s; i++){
			arr[i]= Integer.parseInt(br.readLine());
			sum = sum+arr[i];
		}
		System.out.println("Sum of all elements = "+sum);
	}
}

