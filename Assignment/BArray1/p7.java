/* WAP to find common element between 2 arrays.
 */

import java.io.*;
class num{
	public static void main(String[] arg)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size:");
		int size;
	       do{
	       	      size = Integer.parseInt(br.readLine());
		      if(size<=0){
			      System.out.println("Re-enter size of array:");
		      }
	       }while(size<=0);
		int [] arr1 = new int[size];
		int [] arr2 = new int[size];
		System.out.println("Enter elements of 1 array:");
                for(int i=0; i<arr1.length; i++){
			arr1[i] = Integer.parseInt(br.readLine());
		}
		System.out.println("Enter elements of 2 array:");
		for(int i=0; i<arr2.length; i++){
			arr2[i] = Integer.parseInt(br.readLine());
		}
		for(int i=0; i<arr1.length; i++){
			for(int j=0; j<arr2.length; j++){
			if(arr1[i] == arr2[j]){
				System.out.println("Common element = "+arr1[i]);
			}
		}
	}
 }
}
