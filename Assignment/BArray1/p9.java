/* WAP to merge 2 arrays.
 */
  
import java.io.*;
class merge{
	public static void main(String[] arg)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size of first array:");
		int s1 = Integer.parseInt(br.readLine());
		int arr1[] = new int[s1];
		System.out.println("Enter elements of first array:");
		for(int i=0; i<arr1.length; i++){
			arr1[i] = Integer.parseInt(br.readLine());
		}
		System.out.println("Enter size of second array:");
                int s2 = Integer.parseInt(br.readLine());
                int arr2[] = new int[s2];
                System.out.println("Enter elements of second array:");
                for(int i=0; i<arr2.length; i++){
                        arr2[i] = Integer.parseInt(br.readLine());
                }
		int s3 = s1 +s2;
		int arr3[] = new int[s3];
		int x=0,y=0;
		while(x<arr1.length){
			arr3[x]=arr1[x];
			x++;
		}
		while(x<arr3.length){
			arr3[x]=arr2[y];
			x++;
			y++;
		}

		System.out.println("The merge array:");
		for(int i=0; i<arr3.length; i++){
			System.out.print(arr3[i]+"\t");
		}
		System.out.println();
	}
}
