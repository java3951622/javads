/* WAP to find uncommon elements between 2 arrays.
 */

import java.io.*;
class num{
	public static void main(String[] arg)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size:");
		int size = Integer.parseInt(br.readLine());
		int [] arr1 = new int[size];
		int [] arr2 = new int[size];
		System.out.println("Enter elements of 1 array:");
		for(int i=0; i<arr1.length; i++){
			arr1[i] = Integer.parseInt(br.readLine());
		}
		System.out.println("Enter the elements of 2 array:");
			for(int i=0; i<arr2.length; i++){
			       arr2[i] = Integer.parseInt(br.readLine());
			}
			 System.out.println("Uncommon elements in both arrays:");
			 
        for (int i=0; i<arr1.length; i++) {
		int flag = 0;
            for (int j=0; j<arr2.length; j++) {
                if (arr1[i] == arr2[j]) {
                    flag = 1;
                    break;
                }
            }
            if (flag == 0) {
                System.out.print(arr1[i] + "\t");
            }
        }
        for (int i=0; i<arr2.length; i++) {
            int flag = 0;
            for (int j=0; j<arr1.length; j++) {
                if (arr2[i] == arr1[j]) {
                    flag = 1;
                    break;
                }
            }
            if (flag == 0) {
                System.out.print(arr2[i] + " ");
            }
        }
	System.out.println();
	}
}	

