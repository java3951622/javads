/* WAP to take size & elements from user.Find the minimum elements from array.
 */
 
import java.io.*;
class min{
	public static void main(String[] arg)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the size: ");
		int size = Integer.parseInt(br.readLine());
		int arr[] = new int[size];
		int a=0;
		System.out.println("Enter elements of array:");
		for(int i=0; i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());

		}
		int x = arr[0];
		for(int i=1; i<arr.length; i++){
			if(arr[i]<x){
				x = arr[i];
			}
		}
		System.out.println("Minimum element = "+x);
	}
}

