/* WAP to print elements whose addition of digits is even.
 * ex- 26=2+6=8(8 is even)
 * Input-> 1 2 3 15 36 44 
 * Output-> 2 15 44 
 */

import java.io.*;
class add{
	public static void main(String[] arg)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the size of array:");
		int size;
		do{
	       	  size = Integer.parseInt(br.readLine());
		  if(size<=0)
			  System.out.println("Re-enter size of array:");
		}while(size<=0);
		int [] arr = new int[size];
		System.out.println("Enter array elements:");
		for(int i=0; i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		System.out.println("Elements which addition of digits is even:");
		for(int i=0; i<arr.length; i++){
			int x = arr[i];
			int sum=0;
			while(x!=0){
				int r = x%10;
				sum = sum+r;
				x=x/10;
			}
			if(sum%2 == 0){
				System.out.println(arr[i]+"\t");
			}
		}
	}
}


