/* WAP to search specific elements from array & return its index.
 */

import java.io.*;
class search{
	public static void main(String[] arg)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the size:");
		int size = Integer.parseInt(br.readLine());
		int [] arr = new int[size];
		System.out.println("Enter the elements:");
		for(int i=0; i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		System.out.print("Enter element for search:");
		int x = Integer.parseInt(br.readLine());
		for(int i=0; i<arr.length; i++){
			if(arr[i]==x){
				System.out.println("Element found at = "+i);
				break;
			}
		}
		
	}
}
