/* WAP to find number of odd & even integers in array.
 */

import java.io.*;
class num{
	public static void main(String[] arg)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size of array:");
		int s = Integer.parseInt(br.readLine());
		int arr[] = new int[s];
		int c1=0,c2=0;
		System.out.println("Enter elemnets:");
		for(int i=0; i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
			if(arr[i]%2 == 0){
				c1++;
			}else{
				c2++;
			}
		}
		System.out.println("Even integers = "+c1);
		System.out.println("Odd integers = "+c2);
	}
}

