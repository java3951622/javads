/* Rotate Array
 * Input : A[] = {1,2,3,4,5}
 *         K = 2
 * Output : {3,4,5,1,2}
 */

import java.io.*;
class Rotate{
	static void rotate(int arr[],int n,int k){
		for(int j=1; j<=k; j++){
			int store = arr[0];
			for(int i=0; i<n-1; i++){
				arr[i] = arr[i+1];
			}
			arr[n-1] = store;
		
		}
		System.out.println("After rotating array:");
		for(int i=0; i<arr.length; i++){
			System.out.println(arr[i]);
		}
	}
 public static void main(String[] arg)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter size of array :");
                int n = Integer.parseInt(br.readLine());
                int arr[] = new int[n];
                System.out.println("Enter array elements:");
                for(int i=0; i<arr.length; i++){
                        arr[i] = Integer.parseInt(br.readLine());
                }
		 System.out.println("Enter number for rotate :");
                int k = Integer.parseInt(br.readLine());
		rotate(arr,n,k);
 }
}
