import java.util.*;
class Solution {
    public static int[] nextGreaterElement(int[] nums1, int[] nums2) {
        int arr[] = new int[nums1.length];
        ArrayList al = new ArrayList();
        for(int i=0; i<nums2.length; i++){
            al.add(nums2[i]);
        }

        int k = 0;
        for(int i=0; i<nums1.length; i++){
            int x = al.indexOf(nums1[i]);
	  
            for(int j=x; j<nums2.length; j++){
                if(nums1[i] < nums2[j]){
                    arr[k] = nums2[j];
		    
                    break;
                }
            }
	    k++;
            
        }
        for(int i=0; i<arr.length; i++){
            if(arr[i] == 0){
                arr[i] = -1;
            }
        }
        return arr;
    }
    public static void main(String[] ar){
	    int nums1[] = new int[]{4,1,2};
	    int nums2[] = new int[]{1,3,4,2};
	    int arr[] = nextGreaterElement(nums1,nums2);
	    for(int x:arr){
		    System.out.print(x+" ");
	    }
	    System.out.println();
    }
}
