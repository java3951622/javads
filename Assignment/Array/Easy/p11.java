/* Wava Array
 * */

class Wave{
       void ArrayWave(int arr[]){
	       int n = arr.length;
	       for(int i=0; i<arr.length-1; i=i+2){
		       if(arr[i]<arr[i+1]){
			       int x = arr[i];
			       arr[i] = arr[i+1];
			       arr[i+1] = x;
		       
		       }
	       }
	       for(int i=0; i<n; i++){
		       System.out.print(arr[i] +"  ");
	       }
	       System.out.println();
       }
       public static void main(String[] arg){
	       Wave obj = new Wave();
	       int arr[] = new int[]{2,4,7,8,9,10};
	       obj.ArrayWave(arr);
       }
}


