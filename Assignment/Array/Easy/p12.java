/* Zig zag array
 */
class Zigzag{
       void Arrayzig(int arr[]){
               int n = arr.length;
               for(int i=1; i<arr.length-1; i=i+2){

                       if(arr[i]<arr[i-1]){
                               int x = arr[i];
                               arr[i] = arr[i-1];
                               arr[i-1] = x;

                       }
		       if(arr[i]<arr[i+1]){
			        int x = arr[i];
                               arr[i] = arr[i+1];
                               arr[i+1] = x;
		       }
			       
               }
	       if(n%2 == 0){
		       if(arr[n-1] < arr[n-2]){
			       int x = arr[n-1];
			       arr[n-1] = arr[n-2];
			       arr[n-2] = x;
		       }
	       }
               for(int i=0; i<n; i++){
                       System.out.print(arr[i] +"  ");
               }
               System.out.println();
       }
       public static void main(String[] arg){
               Zigzag obj = new Zigzag();
               int arr[] = new int[]{1,4,3,2};
               obj.Arrayzig(arr);
       }
}
