/* FInd transition point
 * Input : A[] = {0,0,0,1,1}
 * Output : 3
 */
import java.io.*;
class Transition{
static int point(int[] arr, int n){
	int start = 0, end = n - 1;
        while (start <= end) {
            int mid = (start + end) / 2;
            if (arr[mid] == 0)
                start = mid + 1;
            else if (arr[mid] == 1) {
                if (mid == 0|| (mid > 0 && arr[mid - 1] == 0))
                    return mid;
                end = mid - 1;
            }
        }
        return -1;
}
    
 public static void main(String[] arg)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter size of array :");
                int n = Integer.parseInt(br.readLine());
                int arr[] = new int[n];
                System.out.println("Enter array elements:");
                for(int i=0; i<arr.length; i++){
                        arr[i] = Integer.parseInt(br.readLine());
                }
		int x = point(arr,n);
		System.out.println("Transition point is :"+x);
 }
}
