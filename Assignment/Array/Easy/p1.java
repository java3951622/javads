/* Missing number in array
 * Input : A[] = {1,2,4,5,6}
 * Output : 3
 * TC = O(N)
 * SC = O(1)
 */
import java.io.*;
class Missing{
	static void missing(int arr[], int n){
		int sum = ((n+1)*(n+2))/2;
		for(int i=0; i<arr.length; i++){
			sum = sum-arr[i];
		}
		System.out.println("Missing number is :"+sum);
	}

 public static void main(String[] arg)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter size of array :");
                int n = Integer.parseInt(br.readLine());
                int arr[] = new int[n];
                System.out.println("Enter array elements:");
                for(int i=0; i<arr.length; i++){
                        arr[i] = Integer.parseInt(br.readLine());
                }
		missing(arr,n);
 }
}
