/* Check if array is sorted 
 * Input : A[] = {20,30,10,60,40}
 * Output : 0
 * If it sorted then return 1
 */

 import java.io.*;
class Checksort{
	static int ifsort(int arr[]){
		for(int i=0; i<arr.length-1; i++){
			if(arr[i] > arr[i+1]){
				return 0;
			}
		}
		return 1;
	}
 public static void main(String[] arg)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter size of array :");
                int n = Integer.parseInt(br.readLine());
                int arr[] = new int[n];
                System.out.println("Enter array elements:");
                for(int i=0; i<arr.length; i++){
                        arr[i] = Integer.parseInt(br.readLine());
                }
		System.out.println("Result:"+ifsort(arr));
 }
}
