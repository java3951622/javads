/* Alternate positive and negative number
 */
import java.util.*;
class Alternate{
	void rearrange(int arr[], int n){
		 ArrayList<Integer> pos = new ArrayList<Integer>();
                 ArrayList<Integer> neg = new ArrayList<Integer>();
        for(int x=0; x<n; x++){
            if(arr[x] >= 0){
                pos.add(arr[x]);
            }else{
                neg.add(arr[x]);
            }
        }
	int j=0,i=0;
        int k=0;
        while(i< pos.size() && j< neg.size()){
            if( k%2 == 0){
                arr[k] = pos.get(i);
		i++;
            }else{
		    arr[k] = neg.get(j);
		    j++;
	    }
	    k++;
       }
       while(i<pos.size()){
	       arr[k] = pos.get(i);
	       i++;
	       k++;
       }
        while(j<neg.size()){
               arr[k] = neg.get(j);
               j++;
               k++;
       }
       for(int x=0; x<n; x++){
	       System.out.print(arr[x]+"  ");
       }
       System.out.println();
     }
public static void main(String[] arg){
	int arr[] = new int[]{-5, -2, 5, 2, 4, 7, 1, 8, 0 ,-8};
	Alternate obj = new Alternate();
	obj.rearrange(arr, arr.length);
}
}
