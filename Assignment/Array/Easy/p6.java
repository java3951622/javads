/* Second largest element
 * Input : A[] = {12,35,1,10,34}
 * Output : 34
 */

import java.io.*;
class SecMax{
	static void max(int arr[]){
		int min = arr[0];
		for(int i=1; i<arr.length; i++){
			if(arr[i]<min)
				min = arr[i];
		}
		int l1 = min;
		int l2 = min;
		for(int i=0; i<arr.length; i++){
			if(arr[i] > l1){
				l2 = l1;
				l1 = arr[i];
			}
		        if(arr[i] > l2 && arr[i] != l1){
				l2 = arr[i];
			}
		}
		System.out.println("Second large element is: "+l2);
	}
 public static void main(String[] arg)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter size of array :");
                int n = Integer.parseInt(br.readLine());
                int arr[] = new int[n];
                System.out.println("Enter array elements:");
                for(int i=0; i<arr.length; i++){
                        arr[i] = Integer.parseInt(br.readLine());
                }
		max(arr);
 }
}
