/* Sort array 0s,1s,2s
 * Input : A[] = {0,2,1,2,0}
 * Output : {0,0,1,2,2}
 */
import java.io.*;
class sortA{
	static void sortA(int arr[]){
		int c1=0,c2=0,c3=0;
		for(int i=0; i<arr.length; i++){
			if(arr[i] == 0)
				c1++;
			if(arr[i] == 1)
				c2++;
			if(arr[i] == 2)
				c3++;
		}
		for(int i=0; i<c1; i++){
			arr[i] = 0;
		}
		for(int i=c1; i<c1+c2; i++){
			arr[i] = 1;
		}
		for(int i=c1+c2; i<arr.length; i++){

			arr[i] = 2;
		}
		System.out.println("After sorting:");
		for(int i=0; i<arr.length; i++){
			System.out.println(arr[i]);
		}
	}


 public static void main(String[] arg)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter size of array :");
                int n = Integer.parseInt(br.readLine());
                int arr[] = new int[n];
                System.out.println("Enter array elements:");
                for(int i=0; i<arr.length; i++){
                        arr[i] = Integer.parseInt(br.readLine());
                }
		sortA(arr);
 }
}
