/* Search Insert Position (LeetCode-35)
 * Given sorted array of distinct integers & target value return index if target is found. If not,return index where it would be if it were inserted in order.
 * You must write algorithm in O(logn) runtime complexity
 * Input : [1,3,5,6]  , target=5
 * Output : 2
 * Input : [1,3,5,6]  , target=7
 * Output : 4
 * Constraints -  1<=num.length<=104
 *                -104<=nums[i]<=104
 *                nums contains distinct values sorted in ascending order
 *                -104<=target<=104
 *
 */ 
import java.io.*;
class insert{
	static int insert(int arr[],int t){
		int s = 0,e=arr.length;
		while(s<e){
			int m = (s+e)/2;
			if(arr[m] <t){
				s = m+1;
			}else if(arr[m]>t){
				e = m-1;
			}else{
				return m;
			}
		}
		return e;
	}
 public static void main(String[] arg)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter the size:");
                int s = Integer.parseInt(br.readLine());
                int arr[]  = new int[s];
                System.out.println("Enter array elements:");
                for(int i=0; i<arr.length; i++){
                        arr[i] = Integer.parseInt(br.readLine());
                }
		 System.out.println("Enter the target:");
                int t = Integer.parseInt(br.readLine());
		System.out.println("Position :"+insert(arr,t));
 }
}
