/* Reverse Integer (LeetCode-7)
 * Given signed 32-bit integer x, return x with its digit reversed,if reversing x causes value to go outside the signed 32-bit integer range [-231,231-1], then return 0.
 * Assume environment  does not allow you to store 64-bit integers(signed or unsigned).
 * Input : x = 123
 * Output : 321
 * Input : x = -123
 * Output : -321
 *
 * Constraint -
 *  -231<=x<=231-1
 */

import java.io.*;
class reverse{
	static int reverse(int num){
	   
            if (num <= 1) {
                return num;
              }
                return Integer.parseInt(num % 10+"" + reverse(num / 10));
              }
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the number:");
		int num = Integer.parseInt(br.readLine());
		if(num < 0){

			System.out.println("The number :-"+(reverse(-num)));
		}else{
			  System.out.println("The number :"+(reverse(num)));
		}
	}
}
