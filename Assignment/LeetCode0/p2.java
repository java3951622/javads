/* Two Sum (Leetcode - 1)
 * Given array of integer numbers & integer targer, return indices of two numbers such that they add up to target.
 * You may assume that each input would have exactly one solution & you may not use same element twice.
 * You can return answer in ay order
 * Input : [2,7,11,15]  target=9
 * Output : [0,1]
 * Constraints- 2<=num.length<=104
 *              -109<=nums[i]<=109
 *              -109<=target<=109
 *              Only one valid answer exists.
 */

import java.io.*;
class sum{
	static void sum(int arr[], int t){
		for(int i=0; i<arr.length; i++){
			for(int j=i+1; j<arr.length; j++){
				if(arr[i]+arr[j] == t){
					System.out.println("Output:"+" "+"["+i+","+j+"]");
					break;
				}
			
			}
		}
	}
	public static void main(String[] arg)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the size:");
		int s = Integer.parseInt(br.readLine());
		int arr[]  = new int[s];
		System.out.println("Enter array elements:");
		for(int i=0; i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		 System.out.println("Enter the target:");
                int t = Integer.parseInt(br.readLine());
		sum(arr,t);
	}
}
