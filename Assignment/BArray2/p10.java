/* WAP to print second min element in the array.
 * I/P-> 255	2	4	96	34
 * O/P-> 4
 */

import java.io.*;
class secmin{
	static int secmin(int arr[]){
		
		for(int i=0; i<arr.length; i++){
			for(int j=i+1; j<arr.length; j++){
			if(arr[i]>arr[j]){
				int sec = arr[i];
				arr[i] = arr[j];
				arr[j] = sec;
			}
		}
	}
	return arr[1];
}
public static void main(String[] arg)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int size;
		System.out.println("Enter the size of array:");
		do{
			size = Integer.parseInt(br.readLine());
			if(size<=0){
				System.out.println("Re-enter the size of array:");
			}
		}while(size<=0);
	int [] arr = new int[size];
		System.out.println("Enter the array elements:");
		for(int i=0; i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		int x = secmin(arr);
		System.out.println("The second minimum element in array is "+x);
	}
}



