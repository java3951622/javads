/* WAP to find composite number from an array & return its index.Take Size & elements from user.
 */


import java.io.*;
class composite {
	static void composite(int arr[],int size){
		for(int i=0; i<arr.length; i++){
			int count = 0;
			for(int j=1; j<arr[i]; j++){
				if(arr[i]%j == 0){
					count++;
				}
				if(count >2){
					System.out.println("Composite "+arr[i]+" found at index "+i);
					break;
				}
			}
		}
	}
	public static void main(String[] arg)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int size;
		System.out.println("Enter the size of array:");
		do{
			size = Integer.parseInt(br.readLine());
			if(size<=0){
				System.out.println("Re-enter the size:");
			}
		}while(size<=0);
		int arr[] = new int[size];
		System.out.println("Enter array elements:");
		for(int i=0; i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
         	composite(arr,size);
	}
}
