/* WAP to find strong number from an array & return its index,
 * I/P-> 10	24	145	234
 * O/P-> Strong no 145 found at index: 2. 
 */

import java.io.*;
class strong{
	static void strong(int arr[]){
		for(int i=0; i<arr.length; i++){
			int x = arr[i];
			int sum = 0;
			while(x!=0){
				int r = x%10;
				int m = 1;
				for(int j=1; j<=r; j++){
					m = m*j;
				}
				sum = sum+m;
				x = x/10;
			}
			if(sum == arr[i]){
				System.out.println("Strong number "+arr[i]+" found at index: "+i);
			}
		}
	}

	public static void main(String[] arg)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int size;
		System.out.println("Enter the size of array:");
		do{
			size = Integer.parseInt(br.readLine());
			if(size<=0){
				System.out.println("Re-enter the size of array:");
			}
		}while(size<=0);
	   int arr[] = new int[size];
	 System.out.println("Enter the elements of array:");
	 for(int i=0; i<arr.length; i++){
		 arr[i] = Integer.parseInt(br.readLine());
	 }
	 strong(arr);
	}
	}


