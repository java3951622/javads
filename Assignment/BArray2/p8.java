/* WAP to find an Armstrong number from an array & return its index.TAKE size & elements from user.
 * I/P-> 10	252	153	36	89
 * O/P-> Armstrong number 153 found at index: 4.
 */

import java.io.*;
class armstrong{
	static void armstrong(int arr[]){
		for(int i=0; i<arr.length; i++){
			int x = arr[i];
			int y = x;
			int sum = 0;
			int c = 0;
			while(x!=0){
				int r = x%10;
				c++;
				x = x/10;
			}
			while(y!=0){
			int r = y%10;
			int mult=1;
			for(int j=1; j<=c; j++){
				 mult = mult*r;
			}
			sum = sum+mult;
			y = y/10;
			}
			if(sum == arr[i]){
				System.out.println("Armstrong number "+arr[i]+" found at index: "+i);
		}
		}
	}
	public static void main(String[] arg)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int size;
		System.out.println("Enter the size:");
		do{
			size = Integer.parseInt(br.readLine());
			if(size<=0){
				System.out.println("Re-enter size of array:");
			}
		}while(size<=0);
		int [] arr = new int[size];
		System.out.println("Enter the array elements:");
		for(int i=0; i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		armstrong(arr);
	}
}




