/* WAP to print second max element in the array.
 * I/P-> 2	255	1554	657
 * O/P-> 657
 */

import java.io.*;
class secmax{
	static int secmax(int arr[]){
		for(int i=0; i<arr.length; i++){
			for(int j=i+1; j<arr.length; j++){
			if(arr[i] > arr[j]){
				int sec = arr[i];
				arr[i] = arr[j];
				arr[j] = sec;
			}
		}
		}
		return arr[arr.length-2];
	
	}
	public static void main(String[] arg)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int size;
		System.out.println("Enter the array size:");
		do{
			size = Integer.parseInt(br.readLine());
			if(size<=0){
				System.out.println("Enter correct array size:");
			}
		}while(size<=0);
		int [] arr = new int[size];
		System.out.println("Enter the array elements:");
				for(int i=0; i<arr.length; i++){
					arr[i] = Integer.parseInt(br.readLine());
				}
				if(size>2){

				int x = secmax(arr);
				System.out.println("The second maximum element in an array is "+x);
				}else{
					System.out.println("Entered size of "+size);
				}
	}
	}
