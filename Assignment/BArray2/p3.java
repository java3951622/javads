/* WAP to reverse each element in array.
 * I/P-> 10	23	253	87
 * O/P-> 01	32	352	78
 */

import java.io.*;
class reverse{
	static void reverse(int arr[]){
		    for(int i=0; i<arr.length; i++){
			   int s = 0;
			  while(arr[i]!=0){
				 int r = arr[i]%10;
				 s = s*10+r;
			         arr[i]=arr[i]/10;
			  }
		          arr[i] = s;
		    }
	}
     public static void main(String[] arg)throws IOException{
          BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	  int size;
	  System.out.println("Enter the size:");
	  do{
		  size = Integer.parseInt(br.readLine());
		  if(size<=0){
			  System.out.println("Re-Enter the size:");
		  }
	  }while(size<=0);
	  int [] arr = new int[size];
	  System.out.println("Enter the elements of array:");
	  for(int i=0; i<arr.length; i++){
		  arr[i] = Integer.parseInt(br.readLine());
	  }
	  reverse(arr);
	  System.out.println("The output array:");
	  for(int i=0; i<arr.length; i++){
		 System.out.print(arr[i]+"\t");
	  }
	  System.out.println();
     }
}
