/* WAP to find a palindrome number from an array return its index.Take size & elements from the user.
 * I/P-> 10	252	564	145
 * O/P-> Palindrome no 252 found at index: 1.
 */
 
import java.io.*;
class palindrome{
	static void palindrome(int arr[]){
		for(int i=0; i<arr.length; i++){
			int x = arr[i];
			int sum = 0;
			while(x!=0){
				int r = x%10;
				sum = sum*10+r;
				x=x/10;
			}
			if(sum == arr[i]){
				System.out.println("Palindrome number "+arr[i]+"found at index: "+i);
			}
		}
	}
	public static void main(String[] arg)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int size;
		System.out.println("Enter the size:");
		do{
			size = Integer.parseInt(br.readLine());
			if(size<=0){
				System.out.println("Re-enter the size:");
			}
		}while(size<=0);
		int [] arr = new int[size];
		System.out.println("Enter the elements :");
		for(int i=0; i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		palindrome(arr);
	}
}




