/* WAP to find perfect number from an array & return its index.Take size & elements from user.
 * I/P-> 10	25	252	496	564
 * O/p-> Perfect no 496 found at index:3.
 */
         
import java.io.*;
class perfect{
	static void perfect(int arr[]){
		for(int i=0; i<arr.length; i++){
			int sum = 0;
		       for(int j=1; j<arr[i]; j++){
		         if(arr[i]%j == 0){
				 sum = sum+j;
			 }
		       }
		       if(sum == arr[i]){
			       System.out.println("Perfect number "+arr[i]+" found at index: "+i);
		       }
		}
	}
	public static void main(String[] arg)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int size;
		System.out.println("Enter the size of array:");
		do{
			size = Integer.parseInt(br.readLine());
			if(size<=0){
				System.out.println("RE-Enter the size of array:");
			}
		}while(size<=0);
	int [] arr = new int[size];
	System.out.println("Enter the elements of array:");
        for(int i=0; i<arr.length; i++){
	arr[i] = Integer.parseInt(br.readLine());
	}
        perfect(arr);
	}
}	
		              		  

