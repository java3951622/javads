/* WAP to print count of digits in elements of array.
 * I/P-> 02	255	1553
 * O/P-> 2	3	4
 */

import java.io.*;
class count{
	static void count(int arr[]){
		System.out.println("Count of digits in array element:");
		for(int i=0; i<arr.length; i++){
			int count = 0;
			for(int j=arr[i]; j>0; j=j/10){
				int r = arr[i]%10;
				count++;
			}
			System.out.print(count+"\t");
		}
		System.out.println();
	}
	static public void main(String[] arg)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int size;
		System.out.println("Enter the size:");
		do{
			size = Integer.parseInt(br.readLine());
			if(size<=0){
				System.out.println("Re-enter the size:");
			}
		}while(size<=0);
		int [] arr = new int[size];
		System.out.println("Enter the elements of array:");
		for(int i=0; i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		count(arr);
	}
}
