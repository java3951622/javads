/* Write a Java Program to count the number of words in a string using HashMap.
 * */

import java.util.*;
class WordCount {

    static int countWords(String str) {
        str = str.trim();
        if (str.isEmpty())
            return 0;
        String[] words = str.split("\\s+");


        HashMap<String, Integer> wordCountMap = new HashMap<>();
        for (String word : words) {
            word = word.toLowerCase();
            wordCountMap.put(word, wordCountMap.getOrDefault(word, 0) + 1);
        }
        return wordCountMap.size();
    }

    public static void main(String[] args) {
        String str = "This string test .";
        int wordCount = countWords(str);
        System.out.println("Total number words: " + wordCount);
    }
}

