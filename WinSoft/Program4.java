/*Write a Java Program to find the duplicate characters in a string.
*/

class DuplicateCharacters {
    static void duplicateCharacters(String str){
        str = str.toLowerCase();
        for (int i = 0; i < str.length(); i++) {
            char xChar = str.charAt(i);

            if (Character.isLetter(xChar) && str.indexOf(xChar) != i) {
                System.out.println(xChar);

                str = str.replace(xChar, ' ');
            }
        }
    }

    public static void main(String[] args) {
        String str = "Management";
        duplicateCharacters(str);
    }
}

