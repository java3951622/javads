/* Duplicate values cannot be stored
 */
import java.util.*;
class Demo{
	public static void main(String[] arg){
		IdentityHashMap im = new IdentityHashMap();
		im.put(20,"Tanvi");
		im.put(20,"Payal");
		im.put(20,"Ruchita");
		im.put(20,"Isha");
		System.out.println(im);
	}
}
