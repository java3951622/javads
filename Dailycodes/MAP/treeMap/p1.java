/* tree map
 * print in alphabetical order
 */

import java.util.*;
class treeDemo{
	public static void main(String[] arg){
		TreeMap tm = new TreeMap();
		tm.put("Ind","India");
		tm.put("Pak","Pakistan");
		tm.put("NZ","New Zealand");
		tm.put("Aus","Australia");

		System.out.println(tm);
	}
}
