/* Sort by name using comparator
 */

import java.util.*;
class Platform{
	String str;
	int year;
	Platform(String str, int year){
		this.str = str;
		this.year = year;
	}
	public String toString(){
		return "{"+str+":"+year+"}";
	}
}
class SortByName implements Comparator{
	public int compare(Object obj1, Object obj2){
		return ((Platform)obj1).str.compareTo(((Platform)obj2).str);
	}
}
class Tdemo{
	public static void main(String[] arg){
		TreeMap tm = new TreeMap(new SortByName());
		tm.put(new Platform("YouTube",2005),"Google");
		 tm.put(new Platform("YouTube",2005),"Meta");
		  tm.put(new Platform("YouTube",2005),"Google");

		  System.out.println(tm);
	}
}
