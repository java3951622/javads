/* Using comparable
 */

import java.util.*;
class Platform implements Comparable{
	String str;
	int foundYr;
	Platform(String str, int foundYr){
		this.str = str;
		this.foundYr = foundYr;
	}
	public String toString(){
		return "{"+str+":"+foundYr+"}";
	}
	public int compareTo(Object obj){
		return this.foundYr - ((Platform)obj).foundYr;
	}
}
class treeD{
	public static void main(String[] ARG){
		TreeMap tm = new TreeMap();
		tm.put(new Platform("YouTube",2005),"Google");
		tm.put(new Platform("Facebook",2004),"Meta");
	        tm.put(new Platform("Instagram",2010),"Meta");

		System.out.println(tm);
	}
}
