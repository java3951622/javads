/* Dictionary methods
 */

import java.util.*;
class ddemo{
	public static void main(String[] arg){
		Dictionary dt = new Hashtable();
		dt.put(10,"Sachin");
		dt.put(7,"MSD");
		dt.put(18,"Virat");
		dt.put(1,"KL");

		System.out.println(dt);
		//keys
		Enumeration itr1 = dt.keys();
		while(itr1.hasMoreElements()){
			System.out.println(itr1.nextElement());
		}
		//elements
		Enumeration itr2 = dt.elements();
                while(itr2.hasMoreElements()){
                        System.out.println(itr2.nextElement());
                }
		System.out.println(dt.get(7));
		dt.remove(10);
		System.out.println(dt);
	}
}

