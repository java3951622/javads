

import java.util.*;
class pdemo{
	public static void main(String[] arg){
		Properties obj = new Properties();
		FileInputStream fobj = new FileInputStream("friends.properties");
		obj.load(fobj);
		String name = obj.getProperty("Payal");
		System.out.println(name);
		obj.setProperty("Isha");
		FileOutputStream oobj = new FileOutputStream("friends.properties");
		obj.store(oobj,"Updatedby Tanvi");
	}
}
