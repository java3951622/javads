

import java.util.*;
class Demo{
	String str;
	Demo(String str){
		this.str = str;
	}
	public String toString(){
		return str;
	}
	public void finalize(){
		System.out.println("Notify");
	}
}
class GCdemo{
	public static void main(String[] arg){
		Demo obj1 = new Demo("ABC");
		Demo obj2 = new Demo("PQR");
		Demo obj3 = new Demo("XYZ");
		WeakHashMap hm = new WeakHashMap();
		hm.put(obj1,12);
		hm.put(obj2,24);
	        hm.put(obj3,36);
		obj1 = null;
		System.gc();
		System.out.println(hm);
	}
}
