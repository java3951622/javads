/* Sorted Map methods
 */

import java.util.*;
class Demo{
	public static void main(String[] arg){
		SortedMap sm = new TreeMap();
		sm.put("Ind","India");
	        sm.put("Aus","Australia");
        	sm.put("Pak","Pakistan");
	        sm.put("SL","SriLanka");
        	sm.put("NZ","New Zealand");

		System.out.println(sm);

		//submap()
		System.out.println(sm.subMap("Aus","Pak"));
		//headMap()
		System.out.println(sm.headMap("Pak"));
		//tailMap()
		System.out.println(sm.tailMap("Pak"));
		//firstKey()
		System.out.println(sm.firstKey());
		//lastKey()
		System.out.println(sm.lastKey());
		//keySet()
		System.out.println(sm.keySet());
		//values
		System.out.println(sm.values());
		//entrySet()
		System.out.println(sm.entrySet());
	}
}
