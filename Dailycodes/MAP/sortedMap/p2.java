/* Iterating over map
 */
import java.util.*;
class itrDemo{
	public static void main(String[] arg){
		SortedMap sm = new TreeMap();
		sm.put("Ind","India");
		sm.put("Aus","Australia");
		sm.put("NZ","New Zealand");
		sm.put("Pak","Pakistan");

		System.out.println(sm);
		Set<Map.Entry>data = sm.entrySet();
		Iterator<Map.Entry>itr = data.iterator();
		while(itr.hasNext()){
			Map.Entry abc = itr.next();
			System.out.println(abc.getKey()+":"+abc.getValue());
		}
	}
}
