/* Bubble sort without extra iteration
 */

import java.util.*;
class sort{
	static int count = 0;
	static void bubble(int arr[]){
		
		int m = arr.length;
		for(int i=0; i<m; i++){
			count++;
			int flag = 0;
			for(int j=0; j<arr.length-i-1; j++){
				if(arr[j] > arr[j+1]){
					int x = arr[j];
					arr[j] = arr[j+1];
					arr[j+1] = x;
					flag = 1;
				}
			}
		//	m--;
			if(flag == 0)
				break;
		}
	}
	public static void main(String[] arg){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size of array:");
		int s = sc.nextInt();
		int arr[] = new int[s];
		System.out.println("Eneter array elements:");
		for(int i=0; i<arr.length; i++){
			arr[i] = sc.nextInt();
		}
		bubble(arr);
		System.out.println("After sorting:");
		for(int x : arr){
			System.out.println(x);
		}
		System.out.println("Count :"+count);
	}
}
