/* Bubble sort using recursion
 */

import java.util.*;
class rec{
	
	static void bubble(int arr[], int n){
		if(n==1)
			return;
		for(int i=0; i<arr.length-1; i++){
			if(arr[i] > arr[i+1]){
				int x = arr[i];
				arr[i] = arr[i+1];
				arr[i+1] = x;
			}
			}
			 bubble(arr,n-1);
		}
 public static void main(String[] arg){
                Scanner sc = new Scanner(System.in);
                System.out.println("Enter size of array:");
                int s = sc.nextInt();
                int arr[] = new int[s];
                System.out.println("Eneter array elements:");
                for(int i=0; i<arr.length; i++){
                        arr[i] = sc.nextInt();
                }
                bubble(arr,arr.length);
                System.out.println("After sorting:");
                for(int x : arr){
                        System.out.println(x);
                }
               
        }
}
