//Calculate bill amount
//unit<=100 : price per unit is 1
//unit>100 : price per unit is 2

class Demo{
	public static void main(String[] args){
		int unit=350;
		if(unit<=100){
			System.out.println(unit*1);
		}else{
			System.out.println((unit-100)*2+100);
		}
	}
}
