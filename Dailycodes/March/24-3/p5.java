/* A 1 B 2
 * C 3 D
 * E 4
 * F
 */

class alpha{
	public static void main(String[] arg){
		int r=4,n=1;
		char ch='A';
		for(int i=1; i<=r; i++){
			for(int j=1; j<=r-i+1; j++){
				if(j%2==1){
					System.out.print(ch+"\t");
					ch++;
				}else{
					System.out.print(n+"\t");
					n++;
				}
			}
			System.out.println();
		}
	}
}
