/* 1
 * 2	c
 * 4	e	6
 * 7	h	9	j
 */

class demo{
	public static void main(String[] arg){
		int n=1;
		char ch='a';
		for(int i=1; i<=4; i++){
			for(int j=1; j<=i; j++){
				if(j%2==1){
					System.out.print(n+"\t");
				}else{
					System.out.print(ch+"\t");
				}
				n++;
				ch++;
			}
			System.out.println();
		}
	}
}

