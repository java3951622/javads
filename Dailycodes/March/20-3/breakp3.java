//If number is divisible by 3 & 5 break the loop.
import java.util.*;
class number{
	public static void main(String[] ar){
         Scanner sc=new Scanner(System.in);
	 System.out.println("Enter the range :");
	 int n=sc.nextInt();
	 for(int i=1; i<=n; i++){
		 if(i%3==0 && i%5==0){
			 break;
		 }
		 System.out.println(i);
	 }
	}
}
