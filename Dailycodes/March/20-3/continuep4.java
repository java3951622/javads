import java.util.*;
class Demo{
	public static void main(String[] a){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter number ");
		int n=sc.nextInt();
		for(int i=1; i<=n; i++){
			if((i%3==0 && i%5==0) || i%4==0){
				continue;
			}
			System.out.println(i);
		}
	}
}

