//Print odd integer from 1 to 10 using while loop.

class Demo{
	public static void main(String[] args){
		int n=1;
		while(n<=10){
			if(n%2!=0){
				System.out.println(n);
			}
			n++;
		}
	}
}
