//Print multiple of 4 in given range.

class Demo{
	public static void main(String[] args){
		int n=1;
		while(n<=30){
			if(n%4==0){
				System.out.println(n);
			}
			n++;
		}
	}
}
