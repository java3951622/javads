//Print no. from 8 to 1 in reverse order.

class Demo{
	public static void main(String[] args){
		int n=8;
		while(n>=1){
			System.out.println(n);
			n--;
		}
	}
}
