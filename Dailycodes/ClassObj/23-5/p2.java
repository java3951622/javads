//Static block merge all static block at compile time
//

class client{
	static{
		System.out.println("Static block 1");
	}
	public static void main(String[] arg){
	 System.out.println("In main");
	}
	static{
		 System.out.println("Static block 2");
	}
}
