// Instance block
//


class demo{
	int x = 10;
	demo(){
		System.out.println("Constructor");
	}
	{
		System.out.println("Instance block");
	}
	static {
		System.out.println("Static block");
	}
	public static void main(String[] arg){
		demo obj = new demo();
		 System.out.println("In main");
	}
}
