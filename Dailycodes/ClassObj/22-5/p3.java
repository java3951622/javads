/* system.exit(0) call for ending method in static block
 */

class Demo{
	static{
		System.out.println("This is static block");
		System.exit(0);
	}
}
