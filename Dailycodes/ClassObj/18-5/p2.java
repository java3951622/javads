/*
 */

class employee{
	int empId = 10;
	String name = "Kanha";
	static int y = 50;
	void empInfo(){
		System.out.println("Id = "+empId);
		System.out.println("Name = "+name);
		System.out.println("Y = "+y);
	}
}
class maind{
	public static void main(String[] arg){
		employee emp1 = new employee();
		employee emp2 = new employee();
		emp1.empInfo();
		emp2.empInfo();
		System.out.println("--------------------");
		emp2.empId = 20;
		emp2.name = "Rahul";
		emp2.y = 500;
		emp1.empInfo();
		emp2.empInfo();
	}
}
