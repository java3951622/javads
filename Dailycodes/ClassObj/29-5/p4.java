class demo{
	int x = 20;
	demo(){
		System.out.println("In no-arg constructor");
	}
	demo(int x){
		this();
		System.out.println("In para constructor");
	}
	public static void main(String[] arg){
		demo obj = new demo(10);
	}
}
