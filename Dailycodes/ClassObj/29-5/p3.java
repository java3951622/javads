class demo{
	int x = 10;
	demo(){
		System.out.println("In no-arg constructor");
		System.out.println(x);
	}
	demo(int x){
		System.out.println("In para constructor");
		System.out.println("Local variable = "+x);
		System.out.println("Instance variable = "+this.x);
	}
	public static void main(String[] arg){
		demo obj1 = new demo();
		demo obj2 = new demo(20);
	}
}

