/* Real time example
 */

class passenger{
	String pName = "ABC";
	int age = 23;
	char bloodgrp = 'B';
	void ticket(){
		int ticketNo = 100243;
		float price = 25563f;
		System.out.println("Ticket Number = "+ticketNo);
		System.out.println("Ticket price  = "+price);
	}
}
class airline{
	public static void main(String[] arg){
		passenger obj = new passenger();
		System.out.println("Passenger Details:");
		 System.out.println("Passenger Name = "+obj.pName);
                 System.out.println("Age  = "+obj.age);
		 System.out.println("Blood Group = "+obj.bloodgrp);
                 obj.ticket();
	}
}
