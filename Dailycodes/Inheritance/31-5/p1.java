/*Real time example of inheritance
 */

class MiGaircraft{
	MiGaircraft(){
		System.out.println("MiG series produced by Russian Aircraft Corporation");
	}
}
class MiG21 extends MiGaircraft{
	MiG21(){
		System.out.println("MiG21 is child of MiG series");
	}
}
class MiG35 extends MiGaircraft{
	MiG35(){
		 System.out.println("MiG35 is child of MiG series");
	}
}
class country{
	public static void main(String[] arg){
	 MiGaircraft obj = new MiGaircraft();
	 MiG21 obj1 = new MiG21();
	 MiGaircraft obj2 = new MiG35();
	}
}

