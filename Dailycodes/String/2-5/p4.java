/* method - mystrlen
 */

import java.io.*;
class demo{

	static int mystrlen(String str){
		char arr[] = str.toCharArray();
		int count=0;
		for(int i=0; i<arr.length; i++){
			count++;
		}
		return count;
	}

	public static void main(String[] arg)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the string:");
		String str = br.readLine();
		System.out.println(mystrlen(str));
	}
}

