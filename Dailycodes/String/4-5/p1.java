/* Method - mycompareTo
 */
import java.io.*;
class compared{

	static int mystrcompare(String str1, String str2){
		char arr1[] = str1.toCharArray();
		char arr2[] = str2.toCharArray();
                if(arr1.length >arr2.length){
			return ((arr1.length)-(arr2.length));
		}
		for(int i=0; i<arr1.length; i++){
			if(arr1[i] != arr2[i]){
				return (arr1[i]-arr2[i]);
			}
		}
		return 0;
	}
	public static void main(String[] arg)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the first string:");
		String str1 = br.readLine();
		System.out.println("Enter the second string:");
		String str2 = br.readLine();
		System.out.println(mystrcompare(str1, str2));
	}
}
