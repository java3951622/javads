/* Compare length of two strings
 */
import java.io.*;
class complen{

	static int mystrlen(String str){
		char arr[] = str.toCharArray();
		int count=0;
		for(int i=0; i<arr.length; i++){
			count++;
		}
		return count;
	}
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the first string:");
		String str1 = br.readLine();
		System.out.println("Enter the second string:");
		String str2 = br.readLine();
		if(mystrlen(str1) == mystrlen(str2)){
			System.out.println("Equal length");
		}else{
			System.out.println("Unequal length");
		}
	}
}

