/* Singleton Design Pattern:
 * The class which have make only one object is called Singleton.
 * If we try to create multiple object but only one is create. 
 */
class Singleton{
	static Singleton obj = new Singleton();
	private Singleton(){
		System.out.println("Constructor");
	}
	static Singleton getObject(){
		return obj;
	}
}
class Client{
	public static void main(String[] arg){
		Singleton obj1 = Singleton.getObject();
		System.out.println(obj1);
		Singleton obj2 = Singleton.getObject();
		System.out.println(obj2);
	}
}
