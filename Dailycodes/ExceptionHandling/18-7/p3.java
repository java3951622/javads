/* Userdefined Exception class
 */

import java.util.*;
class DataOverFlow extends RuntimeException{
	DataOverFlow(String msg){
		super(msg);
	}
}
class DataUnderFlow extends RuntimeException{
	DataUnderFlow(String msg){
		super(msg);
	}
}
class ArrayDemo{
	public static void main(String[] arg){
	       Scanner sc = new Scanner(System.in);
              int arr[] = new int[5];
              System.out.println("Enter the array elements:");
              System.out.println("Note : 0<element<100");
              for(int i=0; i<arr.length; i++){
	      int data = sc.nextInt();
              if(data<0){
	              throw new DataOverFlow("Data is less than 0");
	       }
              if(data>100){
	              throw new DataUnderFlow("Data is greater than 100");
	      }
	      }
              for(int i=0; i<arr.length; i++){
	            System.out.print(arr[i]);
	      }
	      }
}
	

