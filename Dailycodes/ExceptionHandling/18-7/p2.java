/* throw in exception handling
 */
import java.util.Scanner;
class Demo{
	public static void main(String[] arg){
		Scanner sc = new Scanner(System.in);
		int x = sc.nextInt();
		try{
			if(x==0){
				throw new ArithmeticException("Divide by zero");
			}
			System.out.println(10/x);
		}catch(ArithmeticException obj){
			System.out.println("Exception int thread"+ Thread.currentThread().getName());
			System.out.println(obj.toString());
			/* or
			obj.printStackTrace();
			*/
		}
	}
}
