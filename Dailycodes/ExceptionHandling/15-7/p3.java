/* Finally Block : By using the finally close the connection of server or socket.
 */

class Demo{
	void m1(){
	}
	void m2(){
	}
	public static void main(String[] arg){
		Demo obj = new Demo();
		obj.m1();
		obj = null;
		try{
			obj.m2();
		}catch(Throwable e){
			System.out.println("Exception");
		}finally{
			System.out.println("Connection closed");
		}
		System.out.println("End main");
	}
}
