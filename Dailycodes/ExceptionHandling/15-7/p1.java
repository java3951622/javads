/* Arithmetic Exception with try & catch
 */

  class Demo{
	  public static void main(String[] arg){
		  System.out.println("Start main");
		  try{
			  System.out.println(30/0);                  //Risky Code
		  }catch(ArithmeticException obj){                   // catch(Exception/Throwable/Object)
			  System.out.println("Exception occured");  //Handling Code
		  }
		  System.out.println("End main");

	  }
  }
