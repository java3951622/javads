/*  Default Exception Handler:
 *  1)Thread Name
 *  2)Exception Name,Class name
 *  3)Description
 *  4)Stack Trace
 */

class Demo{
	void m1(){
		System.out.println(10/0);
		m2();
	}
	void m2(){
		System.out.println("In m2");
	}
	public static void main(String[] arg){
		System.out.println("Start main");
	Demo obj = new Demo();
	obj.m1();

	}
}
