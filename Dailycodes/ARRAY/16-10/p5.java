/* Find total sum of all subarray
 */
import java.io.*;
class subs{
	static int sum(int arr[], int n){
		int ts = 0;
		for(int i=0; i<n; i++){
			int s =0;
			for(int j=i; j<n; j++){
				s = s+arr[j];
				ts = ts+s;
			}
		
		}
		return ts;
	}
	 public static void main(String[] arg)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter size of array:");
                int s = Integer.parseInt(br.readLine());
                int arr[] = new int[s];
                System.out.println("Enter array elements :");
                for(int i=0; i<s; i++){
                  arr[i] = Integer.parseInt(br.readLine());
                }
		System.out.println("Toatal Sum is :"+sum(arr,s));
	 }
}
