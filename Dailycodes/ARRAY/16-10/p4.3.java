/* Print sum of every single sub array with carry forward approach
 * Time complexity = O(n^2)
 * Space complexity = O(1)
 */

import java.io.*;
class sum{
	static void sum(int arr[], int n){
		for(int i=0; i<n; i++){
			int s = 0;
			for(int j=i; j<n; j++){
				s = arr[j]+s;
				System.out.print(s+"\t");
			}
			System.out.println();
		}
	}
	 public static void main(String[] arg)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter size of array:");
                int s = Integer.parseInt(br.readLine());
                int arr[] = new int[s];
                System.out.println("Enter array elements :");
                for(int i=0; i<s; i++){
                  arr[i] = Integer.parseInt(br.readLine());
                }
		sum(arr,s);
	 }
}
