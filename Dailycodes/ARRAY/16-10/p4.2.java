/* Print sum of every single subarray using prefix sum
 * Time complexity = O(N^2)
 * Space complexity = O(1)
 */

import java.io.*;
class prefsum{
	static void sum(int arr[],int n){
		for(int i=1; i<arr.length; i++){
			arr[i] = arr[i]+arr[i-1];
		}
		for(int i=0; i<n; i++){
			for(int j=i; j<n; j++){
				int s = 0;
				if(i != 0)
					
					 s = arr[j]-arr[i-1];
				else
					  s = arr[j];
				System.out.println("Sum :"+s);
			}
		}
	}
	 public static void main(String[] arg)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter size of array:");
                int s = Integer.parseInt(br.readLine());
                int arr[] = new int[s];
                System.out.println("Enter array elements :");
                for(int i=0; i<s; i++){
                  arr[i] = Integer.parseInt(br.readLine());
                }
		sum(arr,s);
	 }
}
