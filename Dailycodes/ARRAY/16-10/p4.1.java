/* Print sum of every single subarray
 * Time Complexity = O(N^3)
 * Space Complexity = O(1)
 */

import java.io.*;
class sum{
	static void sum(int arr[], int n){
		System.out.println("Sum of all sub arrays:");
		for(int i=0; i<n; i++){
			for(int j=i; j<n; j++){
				int s = 0;
				for(int k=i; k<=j; k++){
					s = s+arr[k];
				}
				System.out.println(s);
			}
		}
	}
	 public static void main(String[] arg)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter size of array:");
                int s = Integer.parseInt(br.readLine());
                int arr[] = new int[s];
                System.out.println("Enter array elements :");
                for(int i=0; i<s; i++){
                  arr[i] = Integer.parseInt(br.readLine());
                }
		sum(arr,s);
	 }
}
