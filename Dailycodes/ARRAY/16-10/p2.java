/* Print subarray between start & end index
 */

import java.io.*;
class index{
	static void subarr(int arr[], int s, int e){
		for(int i=s; i<=e; i++){
			System.out.print(arr[i]+"\t");
		}
		System.out.println();
	}
	public static void main(String[] arg)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter size of array:");
                int s = Integer.parseInt(br.readLine());
                int arr[] = new int[s];
                System.out.println("Enter array elements :");
                for(int i=0; i<s; i++){
                  arr[i] = Integer.parseInt(br.readLine());
                }
		 System.out.println("Enter start:");
                int a = Integer.parseInt(br.readLine());
		 System.out.println("Enter end:");
                int b = Integer.parseInt(br.readLine());
                subarr(arr,a,b);
        }
}

