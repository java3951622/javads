/* Total number of subarray possible in array
 * A[] = {1,2,3}
 * O/P = 6
 */

import java.io.*;
class countsub{
        static int count(int n){
               
                return (n*(n+1))/2;
        }
        public static void main(String[] arg)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter size of array:");
                int s = Integer.parseInt(br.readLine());
                int arr[] = new int[s];
                System.out.println("Enter array elements :");
                for(int i=0; i<s; i++){
                  arr[i] = Integer.parseInt(br.readLine());
                }
                System.out.println("Count of subarray:"+count(s));
        }
}
~
