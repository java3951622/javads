/* Print all subarrays in array
 */

import java.io.*;
class subarr{
	static void sub(int arr[]){
		System.out.println("Sub arrays are:");
	       for(int i=0; i<arr.length; i++){
	         for(int j=i; j<arr.length; j++){
	           for(int k=i; k<=j; k++){
			 if(k != j){
	                 System.out.print(arr[k]+",");
			 }else{
		         System.out.print(arr[k]+"\t");
			 }		 

		   }
                   System.out.println();
		 }
	       }
	}
         public static void main(String[] arg)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter size of array:");
                int s = Integer.parseInt(br.readLine());
                int arr[] = new int[s];
                System.out.println("Enter array elements :");
                for(int i=0; i<s; i++){
                  arr[i] = Integer.parseInt(br.readLine());
                }	
		sub(arr);
	 }
}
