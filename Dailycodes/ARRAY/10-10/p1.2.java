/* Equilibrium Index
 * Equilibrium index is an index such that sum of elements at lower indexed is equal to the elements at higher indexes.
 * If no equilibrium index then return -1.
 * 1<=N<=105
 * -105<=A[i]<=105
 *  Time Complexity = O(N)
 */

import java.io.*;
class equilibrium{
	static int equil(int arr[], int n){
		for(int i=1; i<n; i++){
			arr[i] = arr[i-1]+arr[i];
		}
		int e = n-1;
		for(int i=0; i<n; i++){
			if(i == 0){
				if((arr[e]-arr[0]) == 0){
					return i;
				}
			}else if(i == e){
				if(arr[i-1] == 0){
					return i;
				}
			}else{
				if(arr[i-1] == (arr[e]-arr[i])){
					return i;
				}
			}
		}
		return -1;
	}

  public static void main(String[] arg)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter size of array :");
                int s = Integer.parseInt(br.readLine());
                int arr[] = new int[s];
                System.out.println("Enter array elements:");
                for(int i=0; i<s; i++){
                        arr[i] =  Integer.parseInt(br.readLine());
                }
                System.out.println("Output is :"+equil(arr,s));
  }
}
