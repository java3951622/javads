/* Equilibrium Index
 * Equilibrium index is an index such that sum of elements at lower indexed is equal to the elements at higher indexes.
 * If no equilibrium index then return -1.
 * 1<=N<=105
 * -105<=A[i]<=105
 *  Time Complexity = O(N^2)
 */
import java.io.*;
class equilibrium{
	static int equil(int arr[], int n){
		for(int i=0; i<n; i++){
			int s1 = 0,s2 = 0;
			for(int j=0; j<i; j++){
				s1 = s1+arr[i];
			}
			for(int j=i+1; j<n; j++){
				s2 = s2+arr[i];
			}
			if(s1 == s2){
				return i;
			}
		}
		return -1;
	}
	public static void main(String[] arg)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size of array :");
		int s = Integer.parseInt(br.readLine());
		int arr[] = new int[s];
		System.out.println("Enter array elements:");
		for(int i=0; i<s; i++){
			arr[i] =  Integer.parseInt(br.readLine());
		}
		System.out.println("Output is :"+equil(arr,s));
	}
}
