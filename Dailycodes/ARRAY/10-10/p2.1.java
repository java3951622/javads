/* Return length of smallest subarray which contains both maximum of array & minimum of array.
 * Time Complexity = O(N^2)
 */

import java.io.*;
class subarr{
	static void subarr(int arr[], int n){
		int max = Integer.MIN_VALUE;
		int min = Integer.MAX_VALUE;
		for(int i=0; i<n; i++){
			if(arr[i] < min)
				min = arr[i];
			if(arr[i] > max)
				max = arr[i];
		}
		if(max == min){
			System.out.println("Smallest subarray size is 0.");
		}
		int minlen = Integer.MAX_VALUE;
		int len = 0;
		for(int i=0; i<n; i++){
			if(arr[i] == max){
		           for(int j=i+1; j<n; j++){
				   if(arr[j] == min){
					   len = j-i+1;
					   if(minlen > len){
						   minlen = len;
					   }
				   }
			   }
			}else if(arr[i] == min){
				for(int j=i+1; j<n; j++){
					if(arr[j] == max){
						len = j-i+1;
						if(minlen > len){
							minlen = len;
						}
					}
				}
			}
		}
		System.out.println("Smallest length array is :"+minlen);
	}

  public static void main(String[] arg)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter size of array :");
                int s = Integer.parseInt(br.readLine());
                int arr[] = new int[s];
                System.out.println("Enter array elements:");
                for(int i=0; i<s; i++){
                        arr[i] =  Integer.parseInt(br.readLine());
                }
               subarr(arr,s);
  }
}
