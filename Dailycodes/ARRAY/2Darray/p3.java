/* Print the column wise sum of entire matrix
 */

class sum{
        public static void main(String[] arg){
                int arr[][] = new int[][]{{1,2,3,4},{5,6,7,8},{9,10,11,12}};
                for(int i=0; i<arr[0].length; i++){
                        int sum = 0;
                        for(int j=0; j<arr.length; j++){
                                sum = arr[j][i]+sum;
                        }
                        System.out.println(sum);
                }
        }
}	  
