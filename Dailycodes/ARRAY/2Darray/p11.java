/* Given N*N matrix.
 * Print it in spiral fashion
 */


/*class spiral{
 public static void main(String[] arg){
                int arr[][] = new int[][]{{1,2,3,4,5},{6,7,8,9,10},{11,12,13,14,15},{16,17,18,19,20},{21,22,23,24,25}};
		int n = 5;
		int i=0,j=0;
		int c =0;
	        while(n>1){
               
                for(int x = 0; x<arr.length-1; x++){
                        System.out.print(arr[i][j]+"\t");
                        j++;
                }
                 for(int x = 0; x<arr.length-1; x++){
                        System.out.print(arr[i][j]+"\t");
                        i++;
                }
                 for(int x = 0; x<arr.length-1; x++){
                        System.out.print(arr[i][j]+"\t");
                        j--;
                }
                 for(int x = 0; x<arr.length-1; x++){
                        System.out.print(arr[i][j]+"\t");
                        i--;
                }
		c++;
		i++;
		j++;
		n=n-2;
		}
        
	System.out.println();
 }
}
*/
 class Spiral {

    public static void main(String[] args) {

        int arr[][] = new int[][]{{1, 2, 3, 4, 5}, {6, 7, 8, 9, 10}, {11, 12, 13, 14, 15}, {16, 17, 18, 19, 20}, {21, 22, 23, 24, 25}};

        int i = 0;
        int j = 0;
        int N = 5;

        while (N > 1) {
            for (int x = 0; x < N - 1; x++) { // Loop for moving to the right
                System.out.print(arr[i][j] + " ");
                j++;
            }

            for (int x = 0; x < N - 1; x++) { // Loop for moving down
                System.out.print(arr[i][j] + " ");
                i++;
            }

            for (int x = 0; x < N - 1; x++) { // Loop for moving to the left
                System.out.print(arr[i][j] + " ");
                j--;
            }

            for (int x = 0; x < N - 1; x++) { // Loop for moving up
                System.out.print(arr[i][j] + " ");
                i--;
            }

            i++;
            j++;
            N = N - 2; // Reduce N by 2
        }

        if (N == 1) {
            System.out.print(arr[i][j]);
        }
        System.out.println();
    }
}
