/* Given matrix of N*N.
 * Rotate matrix 90 degree clockwise from top right corner
 * Note - No new matrix.
 */

class rotate{
	static void rotate(int arr[][]){
		for(int i=0; i<arr.length; i++){
			for(int j=i+1; j<arr.length; j++){
				int x  = arr[i][j];
				arr[i][j] = arr[j][i];
				arr[j][i] = x;
			}
		}
		for(int i=0; i<arr.length; i++){
			int s = 0, e = arr.length-1;
			while(s<e){
				int x = arr[i][s];
				arr[i][s] = arr[i][e];
				arr[i][e] = x;
				s++;
				e--;
			}
		}
	}
	public static void main(String[] arg){
		int arr[][] = new int[][]{{1,2,3},{4,5,6},{7,8,9}};
		rotate(arr);
		for(int i=0; i<arr.length; i++){
                        for(int j=0; j<arr[i].length; j++){
				System.out.print(arr[i][j]+"\t");
			}
			System.out.println();
		}
	}
}
