/* Given matrix of size n*m .
 * Print all the diagonal(R->L), row consider from 0.
 */

class diagonal{
	public static void main(String[] arg){
		int arr[][] = new int[][]{{1,2,3,4,5,6},{7,8,9,10,11,12},{13,14,15,16,17,18},{19,20,21,22,23,24}};
	        
		for(int k=arr[0].length-1; k>=0; k--){
		int i=0; 
		int j = k;
		while(i<arr.length && j>=0){
			 System.out.print(arr[i][j]+"\t");
			 i++;
			 j--;
		 }
		 System.out.println();
		}
	}
}
	

