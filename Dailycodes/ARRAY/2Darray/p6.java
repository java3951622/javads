/* Given square matrix , print right diagonal
 * Time complexity = O(N)
 */

class rdiag{
	public static void main(String[] arg){
		int arr[][] = new int[][]{{1,2,3},{4,5,6},{7,8,9}};
		int i = 0;
		int j = arr.length-1;
		while( i<arr.length && j>=0){
			System.out.print(arr[i][j]+"\t");
			i++;
			j--;
		
		}
		System.out.println();
	}
}
