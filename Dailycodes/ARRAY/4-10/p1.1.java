/* Build array leftmax of size N.
 * Leftmax of i contains maximum for index 0 to index i.
 * A[] = [-3,6,2,4,5,2,8,-9,3,1]
 * Leftmax = [-3,6,6,6,6,6,8,8,8,8]
 * Time complexity = O(N^2)
 * Space complexity = O(1)
 */

import java.util.*;
class leftmax{
	static void leftmax(int arr[]){
		int max = Integer.MIN_VALUE;
		for(int i=0; i<arr.length; i++){
			for(int j=0; j<=i; j++){
				if(max < arr[j]){
					max = arr[j];
				}
			}
				arr[i] = max;
			
		}
		System.out.println("Leftmax Array:");
		for(int x : arr){

			System.out.print(x+"\t");
		}
		System.out.println();
	}


	public static void main(String[] arg){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter array size:");
		int s = sc.nextInt();
		int arr[] = new int[s];
		System.out.println("Enter array elements:");
		for(int i=0; i<arr.length; i++){
			arr[i] = sc.nextInt();
		}
		leftmax(arr);
	}
}
