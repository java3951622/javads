/* Character array(lower case)
 * Return count of pair(i,j)such that a) i<j
 *                                    b)arr[i]='a'
 *                                    c)arr[j]='g'
 * A[] = {a,b,e,g,a,g}
 * Output : 3
 * Time Complexity = O(N)
 */
import java.util.*;
class character{
	static int pair(char arr[]){
		int pair = 0,counta = 0;
		for(int i=0; i<arr.length; i++){
			if(arr[i] == 'a')
				counta++;
			else if(arr[i] == 'g')
				pair = pair+counta;
		}
		return pair;
	}
  public static void main(String[] arg){
                Scanner sc = new Scanner(System.in);
                System.out.println("Enter array size:");
                int s = sc.nextInt();
                char arr[] = new char[s];
                System.out.println("Enter array elements:");
                for(int i=0; i<arr.length; i++){
                        arr[i] = sc.next().charAt(0);;
                }
                System.out.println("Count is :"+pair(arr));
        }
}
