/* Character array(lower case)
 * Return count of pair(i,j)such that a) i<j
 *                                    b)arr[i]='a' 
 *                                    c)arr[j]='g'
 * A[] = {a,b,e,g,a,g}
 * Output : 3
 * Time Complexity = O(N^2)
 */
import java.util.*;
class count{
	static int character(char arr[]){
		int c =0;
		for(int i=0; i<arr.length; i++){
			if(arr[i] == 'a'){
				for(int j=i+1; j<arr.length; j++){
					if(arr[j] == 'g')
						c++;
				}
			}
		}
		return c;
	}

  public static void main(String[] arg){
                Scanner sc = new Scanner(System.in);
                System.out.println("Enter array size:");
                int s = sc.nextInt();
                char arr[] = new char[s];
                System.out.println("Enter array elements:");
                for(int i=0; i<arr.length; i++){
                        arr[i] = sc.next().charAt(0);;
                }
                System.out.println("Count is :"+character(arr));
        }
}
