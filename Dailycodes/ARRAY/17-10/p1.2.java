 /* Find contiguous subarray (containing atleast 1 no.) which has largest sum & return its sum.
  * Using prefix sum
 * Time Complexity = O(N^2)
 * Space Complexity = O(1)
 */


import java.util.*;
class sum{
        static int subsum(int arr[], int n){
		for(int i=1; i<n; i++){
			arr[i] = arr[i]+arr[i-1];
		}
                int sum = Integer.MIN_VALUE;
                for(int i=0; i<n; i++){
			int s = 0;
                        for(int j=i; j<n; j++){
				if(i == 0)
		         		s = arr[j];
				else
					s = arr[j]-arr[i-1];
                                 if(s >sum)
                                        sum = s;
                                }
                        }
                
                return sum;
   }
        public static void main(String[] arg){
                Scanner sc = new Scanner(System.in);
                System.out.println("Enter array size :");
                int s = sc.nextInt();
                int arr[] = new int[s];
                System.out.println("Enter array elements:");
                for(int i=0; i<s; i++){
                        arr[i] = sc.nextInt();
                }
                System.out.println("Maximum sum subarray:"+subsum(arr,s));
        }
}
                           
