/* Find contiguous subarray (containing atleast 1 no.) which has largest sum & return its sum.
 * Kadane's Algorithm
 * Time Complexity = O(N)
 * Space Complexity = O(1)
 */


import java.util.*;
class sum{
        static int subsum(int arr[], int n){
                int msum = Integer.MIN_VALUE, s = 0;
                for(int i=0; i<n; i++){
                        s = s+arr[i];
                        if(s>msum)
                              msum = s;
			if(s < 0)
				s = 0;
                        }
                        
                return msum;
        }
        public static void main(String[] arg){
                Scanner sc = new Scanner(System.in);
                System.out.println("Enter array size :");
                int s = sc.nextInt();
                int arr[] = new int[s];
                System.out.println("Enter array elements:");
                for(int i=0; i<s; i++){
                        arr[i] = sc.nextInt();
                }
                System.out.println("Maximum sum subarray:"+subsum(arr,s));
        }
}
                           
