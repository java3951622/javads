/* Maxumum subarray sum
 * Find contiguous subarray (containing atleast 1 no.) which has largest sum & return its sum.
 * Time Complexity = O(N^3)
 * Space Complexity = O(1)
 */
 

import java.util.*;
class sum{
	static int subsum(int arr[], int n){
		int sum = Integer.MIN_VALUE;
		for(int i=0; i<n; i++){
			for(int j=i; j<n; j++){
				int s = 0;
				for(int k=i; k<=j; k++){
					s = s+arr[k];
					if(s>sum)
						sum = s;
				}
			}
		}
		return sum;
	}
	public static void main(String[] arg){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter array size :");
		int s = sc.nextInt();
		int arr[] = new int[s];
		System.out.println("Enter array elements:");
		for(int i=0; i<s; i++){
			arr[i] = sc.nextInt();
		}
		System.out.println("Maximum sum subarray:"+subsum(arr,s));
	}
}
