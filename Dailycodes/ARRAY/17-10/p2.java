/* Find contiguous subarray (containing atleast 1 no.) which has largest sum & print that subarray.
 * Time Complexity = O(N)
 * Space Complexity = O(1)
 */


import java.util.*;
class subsum{
        static void subsum(int arr[], int n){
                int maxsum = Integer.MIN_VALUE;
		int sum = 0, s = -1, e = -1;
	        int x = 0;	
                for(int i=0; i<n; i++){
			if(sum == 0){
                            x = i;
			}   
			sum = sum+arr[i];
			if(sum > maxsum){
				maxsum = sum;
				e = i;
				s = x;
			}
			if(sum < 0){
				sum = 0;
			}
		}
		System.out.println("Subarray of max sum is:");
		for(int i=s; i<=e; i++){
		       System.out.print(arr[i]+"\t");
		}	    
	     System.out.println();	
                        
        }
        public static void main(String[] arg){
                Scanner sc = new Scanner(System.in);
                System.out.println("Enter array size :");
                int s = sc.nextInt();
                int arr[] = new int[s];
                System.out.println("Enter array elements:");
                for(int i=0; i<s; i++){
                        arr[i] = sc.nextInt();
                }
                subsum(arr,s);
        }
}
                           
