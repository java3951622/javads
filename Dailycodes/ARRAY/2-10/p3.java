/* Find second largest element in array
 */

import java.io.*;
class secMax{
	static int sec(int arr[]){
		int max = Integer.MIN_VALUE;
		int a = 0;
		for(int i=0; i<arr.length; i++){
			if(arr[i] > max){
				a = max;
				max = arr[i];
			}
		}
		return a;
	}
	 public static void main(String[] arg)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter size:");
                int s = Integer.parseInt(br.readLine());
                int arr[] = new int[s];

                System.out.println("Enter array elements:");
                for(int i=0; i<arr.length; i++){
                        arr[i] = Integer.parseInt(br.readLine());
                }
		System.out.println("Second largest element in array is :"+sec(arr));
	 }
}
