/* Given array of size N.
 * Return count of pairs(i,j)with Arr[i]+Arr[j] = k
 */
import java.io.*;
class pair{  
	static int pair(int arr[],int k){
		int c =0;
		for(int i=0; i<arr.length; i++){
			for(int j=i+1; j<arr.length; j++){
				if(arr[i]+arr[j] == k){
					c++;
				}
			}
		}
		return c*2;
	}
              public static void main(String[] arg)throws IOException{
	
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter size:");
                int s = Integer.parseInt(br.readLine());
                int arr[] = new int[s];

                System.out.println("Enter array elements:");
                for(int i=0; i<arr.length; i++){
                        arr[i] = Integer.parseInt(br.readLine());
                }
		System.out.println("Enter sum:");
		int k = Integer.parseInt(br.readLine());
		System.out.println("The count of pairs:"+pair(arr,k));
	      }
}
