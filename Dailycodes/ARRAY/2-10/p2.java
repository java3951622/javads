/* Given array of size N. Reverse array
 * Time complexity=O(N)
 * Space complexity=O1)
 
 */
import java.io.*;
class reverse{
	static void rev(int arr[], int s){
		int j = arr.length-1;
		for(int i=0; i<s/2; i++){
			arr[i] = arr[i]+arr[j];
			arr[j] = arr[i]-arr[j];
			arr[i] = arr[i]-arr[j];
			j--;
		}
	}
       public static void main(String[] arg)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter size:");
                int s = Integer.parseInt(br.readLine());
                int arr[] = new int[s];

                System.out.println("Enter array elements:");
                for(int i=0; i<arr.length; i++){
                        arr[i] = Integer.parseInt(br.readLine());
                }
		rev(arr,s);
		for(int x : arr){
			System.out.print(x+"\t");
		}
		System.out.println();
       }
}
