/* Prefix sum
 */
import java.io.*;
class prefS{
	public static void main(String[] arg)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size:");
		int s = Integer.parseInt(br.readLine());
		int arr[] = new int[s];

		System.out.println("Enter array elements:");
		for(int i=0; i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		for(int i=1; i<arr.length; i++){
			arr[i] = arr[i]+arr[i-1];
		}
		for(int x : arr){
			System.out.print(x+"\t");
		}
		System.out.println();
	}
}
