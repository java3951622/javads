/* Given array of size N & Q no. of queries.Query contains 2 parameters start & end.
 * Sum of array elements in range of start & end index.
 * Arr = [-3,6,2,4,5,2,8,-9,3,1]
 * N = 10;
 * Q = 3;
 * S  E  sum
 * 1  3  12
 * 2  7  12
 * 1  1   6
 */

import java.util.*;
class sum{
	static void range(int arr[], int Q){
		Scanner sc = new Scanner(System.in);
		for(int i=1; i<=Q; i++){
			System.out.println("Enter range start & end:");
			int a = sc.nextInt();
			int b = sc.nextInt();
			int sum =0;
			for(int j=a; j<=b; j++){
				sum = sum+arr[j];
			}
			System.out.println("Sum in"+a+"and"+b+"is :"+sum);
		}
	}
	public static void main(String[] arg){
		 Scanner sc = new Scanner(System.in);
		int arr[] = new int[]{-3,6,2,4,5,2,8,-9,3,1};
		System.out.println("Enter no. of queries:");
		int Q = sc.nextInt();
		range(arr,Q);
	}
}

