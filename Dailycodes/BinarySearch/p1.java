/* Binary Search in simple way
 */


import java.io.*;
class binary{
	static int binarySearch(int arr[], int k){
		int s = 0, e = arr.length-1;
	       while(s <= e){
	       int mid = (s+e)/2;
                 if(arr[mid] == k){
	             return mid;
		 }
                 if(arr[mid] < k){
	           s = mid+1;
                }else{
                   e = mid-1;
		}
	       }
             return -1;
	}	     
public static void main(String[] arg)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("ENter array size:");
                int s = Integer.parseInt(br.readLine());
                int arr[] = new int[s];
                System.out.println("ENter array elements:");
                for(int i=0; i<arr.length; i++){
                        arr[i] = Integer.parseInt(br.readLine());
                }
                 System.out.println("ENter  element to search:");
                 int k = Integer.parseInt(br.readLine());
                 
                 int ret = binarySearch(arr,k);
                 if(ret == -1)
                         System.out.println("Element not found.");
                 else
                         System.out.println("Element is found at :"+ret);
        }
}
