/* Binary Search in recursion
 */

import java.io.*;
class Binary{
	static int start = 0;
	static int end = 0;
	static int binarySearch(int arr[], int k){
		if(start > end){
			return -1;
		}else{
			int mid = (start+end)/2;
			if(arr[mid] == k){
				return mid;
		       } else if(arr[mid] > k){
				end = mid-1;
				return binarySearch(arr,k);
		       }else{
			       start = mid+1;
			       return binarySearch(arr,k);
		       }
		}
	}
	public static void main(String[] arg)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("ENter array size:");
		int s = Integer.parseInt(br.readLine());
		int arr[] = new int[s];
		System.out.println("ENter array elements:");
		for(int i=0; i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		 System.out.println("ENter  element to search:");
		 int k = Integer.parseInt(br.readLine());
		 end = arr.length-1;
		 int ret = binarySearch(arr,k);
		 if(ret == -1)
			 System.out.println("Element not found.");
		 else
			 System.out.println("Element is found at :"+ret);
	}
}

		


