import java.util.*;
class Node {
        int data;
        Node prev = null;
        Node next = null;
        Node(int data){
                this.data = data;
        }
}
class Linkedlist{
        Node head = null;
        void addFirst(int data){
                Node newNode = new Node(data);
                if(head == null){
                        head = newNode;
			head.prev = head;
			head.next = head;
                }else{
                        newNode.next = head;
                        head.prev.next = newNode;
			newNode.prev = head.prev;
			head.prev = newNode;
                        head = newNode;
                }
        }
        void addLast(int data){
                Node newNode = new Node(data);
                if(head == null){
                        head = newNode;
			head.prev = newNode;
			head.next = head;
                }else{

                        /*Node temp = head;
                        while(temp.next != head){
                                temp = temp.next;
                        }
			newNode.next = head;
                        temp.next = newNode;
                        newNode.prev = temp;
			head.prev = newNode;*/
                        newNode.next = head;
			head.prev.next = newNode;
			newNode.prev = head.prev;
			head.prev = newNode;
                }
        }
        int countNode(){
                if(head == null){
                        return 0;
                }else{
                        int count = 1;
                        Node temp = head;
                        while(temp.next != head){
                                count++;
                                temp = temp.next;
                        }
                        return count;
                }
        }
        void addAtPos(int pos,int data){
                if(head == null && pos!=1){
                        System.out.println("Wrong ip");
                }else{
                        int count = countNode();
                        if(pos <1 || pos >= count+2){
                                System.out.println("Invalid Pos");
                        }else if(pos == 1){
                                addFirst(data);
                        }else if(pos == count+1){
                                addLast(data);
                        }else{
                                Node newNode = new Node(data);
                                Node temp = head;
                                while(pos-2 != 0){
                                        temp = temp.next;
                                        pos--;
                                }
                                newNode.next = temp.next;
                                newNode.next.prev = newNode;
                                temp.next = newNode;
                                newNode.prev = temp;
                        }
                }
        }
        void delFirst(){
                if(head == null){
                        System.out.println("LL is empty");
                }else{

			if(head.next == head){
				head = null;
			}else{
				Node temp = head;
                        head = head.next;
			temp.next.prev = temp.prev;
                      
			head.prev.next = head;
			
			

                }
        }
	}
        void delLast(){
                if(head == null){
                        System.out.println("LL is empty");
                }
                if(head.next == head){
                        head = null;
                }else{
                        head.prev = head.prev.prev;
			head.prev.next = head;
                }
        }
        void delAtPos(int pos){
                if(head == null){
                        System.out.println("LL is empty");
                }else{
                        int count = countNode();
                        if(pos <1 || pos>count){
                                System.out.println("Invalid pos");
                        }else if(pos == 1){
                                delFirst();
                        }else if(pos == count){
                                delLast();
                        }else{
                                Node temp = head;
                               
                                while(pos-2 != 0){
                                        temp = temp.next;
                                        pos--;
                                }
                                temp = temp.next;
                                temp.next.prev = temp.prev;
                                temp.prev.next = temp.next;
                        }
                }
        }
        void printLL(){
                if(head == null){
                        System.out.println("LL is empty");
                }else{
                        Node temp = head;
                        while(temp.next != head){
                                System.out.print(temp.data+" ");
                                temp = temp.next;
                        }
                        System.out.println(temp.data);
                }
        }
}
class user{
        public static void main(String[] arg){
                Scanner sc = new Scanner(System.in);
                Linkedlist ll = new Linkedlist();
                char ch;
                do{
                        System.out.println("1.add first");
                System.out.println("2.add last");
                System.out.println("3.delete first");
                System.out.println("4.add at pos");
                System.out.println("5.delete last");
                System.out.println("6.delete at pos");
                System.out.println("7.count node");
                System.out.println("8.print");

                System.out.println("Enter choice:");
                int choice = sc.nextInt();
                switch(choice){
                        case 1 :{
                                        System.out.println("Enter data");
                                        int x = sc.nextInt();
                                        ll.addFirst(x);
                                }
                                break;
                         case 2 :{
                                        System.out.println("Enter data");
                                        int x = sc.nextInt();
                                        ll.addLast(x);
                                }
                                break;
                         case 3 :{

                                        ll.delFirst();
                                }
                                break;
                         case 4 :{
                                        System.out.println("Enter data");
                                        int x = sc.nextInt();
                                        System.out.println("Enter pos");
                                        int pos = sc.nextInt();
                                        ll.addAtPos(pos,x);
                                }
                                break;
                         case 5 :{

                                        ll.delLast();
                                }
                                break;
                         case 6 :{
                                        System.out.println("Enter pos");
                                        int x = sc.nextInt();
                                        ll.delAtPos(x);
                                }
                                break;
                         case 7 :{
                                         int count =  ll.countNode();
                                        System.out.println(count);

                                }
                                break;
                         case 8 :{

                                        ll.printLL();
                                }
                                break;
                        default :
                                   System.out.println("Wrong ip");
                                   break;
                }
                System.out.println("Do u want continue:");
                ch = sc.next().charAt(0);
		}while(ch == 'Y' || ch == 'y');
	}
}

