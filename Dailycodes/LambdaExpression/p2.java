/* Lambda Expression : Works on only Functional interface
 * Functional interface : Contains only one method
 */

interface Core2Web{
	void lang();
}
class Year2022{
	public static void main(String[] arg){
		/*Core2Web c2w = ()->{
			System.out.println("C,C++,DSA"+"Java");
		};
	        */
		//or
		 Core2Web c2w = ()-> System.out.println("C,C++,DSA"+"Java");
		 
		c2w.lang();
	}
}

