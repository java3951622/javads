/* Lambda function with Comparator
 * Comparator has 2 methods (i.e. compareTo & equal) but equal have body already so it be a functional interface
 */
import java.util.*;
class Employee{
	int empId;
	String name;
	Employee(int empId, String name){
		this.empId = empId;
		this.name = name;
	}
	public String toString(){
		return empId+":"+name;
	}
}
class demo{
	public static void main(String[] arg){
		ArrayList al = new ArrayList();
		al.add(new Employee(21,"Ruchita"));
		 al.add(new Employee(47,"Payal"));
		  al.add(new Employee(45,"Tanvi"));
		   al.add(new Employee(34,"Isha"));

		   System.out.println(al);

		   Collections.sort(al,(obj1,obj2)->{
			   return((Employee)obj1).name.compareTo(((Employee)obj2).name);
		   }
		   );
	}
}
