/* Runnable interface with anonymous inner class
 */

class ThreadDemo{
	public static void main(String[] arg){
		Runnable obj1 = new Runnable(){
			public void run(){
				System.out.println(Thread.currentThread().getName());
			}
		};
		Thread t1 = new Thread(obj1);
		Runnable obj2 = new Runnable(){
			public void run(){
				System.out.println(Thread.currentThread().getName());
			}
		};
		Thread t2 = new Thread(obj2);
		t1.start();
		t2.start();
	}
}
