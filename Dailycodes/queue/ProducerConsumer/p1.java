 
import java.util.concurrent.*;

class Producer implements Runnable{
        BlockingQueue bq = null;
	Producer(BlockingQueue bq){
		this.bq = bq;
	}
	
	public void run(){ 
		for(int i=1; i<10; i++){
			try{
				bq.put(i);
				System.out.println("Produce :"+i);
			}catch(InterruptedException ie){
			}
		}
        }
}
class Consumer implements Runnable{
	BlockingQueue bq = null;
	Consumer(BlockingQueue bq){
		this.bq = bq;
	}
	public void run(){
		for(int i=1; i<10; i++){
			try{
				System.out.println("Consume :"+bq.take());
				
			}catch(InterruptedException ie){
			}
			try{
				Thread.sleep(5000);
			}catch(InterruptedException ie){
			}
		}
	}
}

class produceconsume{
	public static void main(String[] arg){
		BlockingQueue bq = new ArrayBlockingQueue(5);
		Producer produce = new Producer(bq);
		Thread t1 = new Thread(produce);
		t1.start();
		Consumer consume = new Consumer(bq);
		Thread t2 = new Thread(consume);
		t2.start();
	}
}

