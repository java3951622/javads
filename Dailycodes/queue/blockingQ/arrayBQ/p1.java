
/* thread get blocked after adding elements in queue more than size
 */
import java.util.concurrent.*;
class ArraybQ{
	public static void main(String[] arg)throws InterruptedException{
		BlockingQueue bq = new ArrayBlockingQueue(4);
		bq.offer(10);
		bq.offer(20);
		bq.offer(30);
		bq.offer(40);
		System.out.println(bq);
		bq.put(50);
		System.out.println(bq);
	}
}
