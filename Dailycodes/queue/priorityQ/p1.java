/* min heap
 */
import java.util.*;
class Pq{
	public static void main(String[] arg){
		PriorityQueue pq = new PriorityQueue();
		pq.offer(30);
		pq.offer(20);
		pq.offer(10);
		pq.offer(40);
		pq.offer(50);

		System.out.println(pq);
	}
}
