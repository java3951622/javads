/* Method Local Inner clas
 */

class A{
	void m1(){
		System.out.println("In m1 outer");
	    class a{
		    void m1(){
			    System.out.println("In m1-Inner");
		    }
	    }
	    a obj = new a();
	    obj.m1();
	}
        void m2(){
		System.out.println("in m2 outer");
	}
}
 class Client{
	 public static void main(String[] arg){
		 A obj = new A();
		 obj.m1();
		 obj.m2();
	 }
 }
