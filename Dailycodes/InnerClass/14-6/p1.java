/* Inner class :
 *   1)Normal Inner Class
 *   2)Method Local Inner Class
 *   3)Static Inner Class
 *   4)Anonymous Inner Class
 */
// Normal Inner Class

class Outer{
	class Inner{
		void m1(){
			System.out.println("In m1-Inner");
		}
	}
	void m2(){
		System.out.println("In m2-outer");
	}
}
class Client{
	public static void main(String[] arg){
		Outer obj = new Outer();
		obj.m2();
		Outer.Inner obj1 = obj.new Inner();
		obj1.m1();
	}
}
