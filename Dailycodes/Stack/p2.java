/* Implement stack using array
 */
import java.util.*;
class StackD{
	int maxSize;
	int stackArr[];
	int top = -1;
	StackD(int size){
		this.stackArr = new int[size];
		this.maxSize = size;
	}
	void push(int data){
		if(top == (maxSize-1)){
			System.out.println("Stack Overflow");
			return;
		}else{
			top++;
			stackArr[top] = data;
		}
	}
	boolean empty(){
		if(top == -1)
			return true;
		else
			return false;
	}
	void pop(){
		if(empty()){
			System.out.println("Stack is empty");
			return;
		}else{
			int val = stackArr[top];
			top--;
			System.out.println(val+" is popped");
			
		}
	}
	void peek(){
		if(empty()){
			System.out.println("Stack is empty");
		}else{
			System.out.println(stackArr[top]);
		}
	}
	void print(){
		if(empty()){
			System.out.println("Stack is empty");
		}else{
			System.out.print("[");
			for(int i=0; i<=top; i++){
				System.out.print(stackArr[i]+" ");
			}
			System.out.println("]");
		}
	}
}
class Client{
	public static void main(String[] arg){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter stack size:");
		int size = sc.nextInt();
		StackD s = new StackD(size);
		char ch;
		do{
			System.out.println("1.Push");
			System.out.println("2.Pop");
			System.out.println("3.Peek");
			System.out.println("4.Empty");
			System.out.println("5.Print");
			System.out.println("Enter your choice:");
			int choice = sc.nextInt();
			switch(choice){
				case 1: {
						System.out.println("Enter data:");
						int data = sc.nextInt();
						s.push(data);
				        }
				        break;
				case 2: {
						s.pop();
				        }
					break;
				case 3: {
						s.peek();
			        	}
					break;
				case 4: {
						boolean val = s.empty();
					       if(val)
						       System.out.println("Stack is empty");
					       else
						       System.out.println("Stack is not empty");
				        }
					break;
			        case 5: {
						s.print();
					}
					break;
				default:
					System.out.println("Wrong choice");
			}
			System.out.println("Do u want continue:");
			ch = sc.next().charAt(0);
		}while(ch == 'Y'|| ch == 'y');
	}
}
