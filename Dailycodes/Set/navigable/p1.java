/* Navigable Set methods
 */

import java.util.*;
class Demo{
	public static void main(String[] arg){
		NavigableSet ns = new TreeSet();
		ns.add(23);
		ns.add(67);
		ns.add(56);
		ns.add(12);
		ns.add(4);
		System.out.println(ns);
		// E lower(E)
		System.out.println(ns.lower(56));
		// E floor(E)
		System.out.println(ns.floor(65));
		// E ceiling(E)
		 System.out.println(ns.ceiling(45));
		// E higher(E)
		System.out.println(ns.higher(50));
	        // E pollFirst()
		System.out.println(ns.pollFirst());
		// E pollLast()
		System.out.println(ns.pollLast());
	}
}
