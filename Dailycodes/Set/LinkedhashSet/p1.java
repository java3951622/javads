/* Insertion order is preserved in LinkedHashSet
 */

import java.util.*;
class Demo{
	public static void main(String[] arg){
		LinkedHashSet lh = new LinkedHashSet();
		lh.add("Kanha");
		 lh.add("Rahul");
		  lh.add("Ashish");
		   lh.add("Kanha");
		    lh.add("Rahul");
		    System.out.println(lh);
	}
}
