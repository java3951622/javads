import java.util.*;
class CricPlayer{
	int jerNo = 0;
	String pName = null;
	CricPlayer(int jerNo, String pName){
		this.jerNo = jerNo;
		this.pName = pName;
	}
	public String toString(){
		return (pName+" "+jerNo);
	}
}
class Lhs{
	public static void main(String[] arg){
		LinkedHashSet lhs = new LinkedHashSet();
		lhs.add(new CricPlayer(7,"Dhoni"));
		lhs.add(new CricPlayer(18,"Kohli"));
		lhs.add(new CricPlayer(45,"Sharma"));
		lhs.add(new CricPlayer(7,"Dhoni"));
		System.out.println(lhs);
	}
}
