import java.util.*;
class Movies implements Comparable{
	String movieName = null;
	float colltotal = 0.0f;
	Movies(String movieName, float colltotal){
		this.movieName = movieName;
		this.colltotal = colltotal;
	}
	public int compareTo(Object obj){
		return movieName;
	}
}
class TreeDemo{
	public static void main(String[] arg){
		TreeSet ts = new TreeSet();
		ts.add(new Movies("OMG2",200.0f));
		 ts.add(new Movies("Gadar2",200.0f));
		  ts.add(new Movies("Jailer",150.0f));
		  System.out.println(ts);
	}
}
