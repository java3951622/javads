/* methods of File
 */

import java.io.*;
class methods{
	public static void main(String[] arg)throws IOException{
		File obj = new File("Filesystem");
	        obj.createNewFile();
	        //String getName()
		System.out.println(obj.getName());
		//String getParent()
		 System.out.println(obj.getParent());
		//String getPath()
		  System.out.println(obj.getPath());
		//String getAbsolutePath()
		  System.out.println(obj.getAbsolutePath());
		//boolean canRead()
		  System.out.println(obj.canRead());
		//boolean canWrite()
		  System.out.println(obj.canWrite());
		//boolean isDirectory()
		  System.out.println(obj.isDirectory());
		//boolean isFile()
		   System.out.println(obj.isFile());
		   //boolean isHidden()
		    System.out.println(obj.isHidden());
		  // long lastModified()
		    System.out.println(obj.lastModified());  
	}
}

