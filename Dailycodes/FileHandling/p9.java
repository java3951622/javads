/* File Reader
 */

import java.io.*;
class FRdemo{
	public static void main(String[] arg)throws IOException{
		FileReader fr = new FileReader("Incubator.txt");
		int data = fr.read();
		while(data != -1){
			System.out.print((char)data);
			data = fr.read();
		}
		fr.close();
	}
}
