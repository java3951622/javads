/* User defined object arraylist
 */

import java.util.*;
class CricPlayer{
	int jerNo = 0;
	String name = null;
       CricPlayer(int jerNo, String name){
       this.jerNo = jerNo;
       this.name = name;
       }
   public String toString(){
       return name;
    }
}
class Demo{
	public static void main(String[] arg){
		ArrayList al = new ArrayList();
		al.add(new CricPlayer(7,"MSD"));
		al.add(new CricPlayer(18,"Virat"));
		al.add(new CricPlayer(45,"Rohit"));
		al.add(new CricPlayer(12,"Rahul"));
		System.out.println(al);
		al.add(2,new CricPlayer(4,"Raina"));
		System.out.println(al);
	}
}

