
import java.util.*;
class ArrayD{
	public static void main(String[] arg){
		ArrayList al = new ArrayList();
		//add(element)
		al.add(234);
		al.add("A");
		al.add(5.9);
		al.add("tanvi");
		System.out.println(al);
		//int size()
		System.out.println(al.size());
		// boolean contains(Object)
		System.out.println(al.contains(5.9));
		System.out.println(al.contains(10));
                //int indexOf(Object)
		System.out.println(al.indexOf("tanvi"));
                System.out.println(al.indexOf(23));
		al.add(234);
		//int lastIndexOf(Object)
		System.out.println(al.lastIndexOf(234));
		System.out.println(al.lastIndexOf(2));
		//E get(int)
		System.out.println(al.get(0));
		//System.out.println(al.get(6));
		//E set(int, E)
		al.set(4,39);
		System.out.println(al);
	}
}
