/* Concurrency methods in thread class :
 * 1) Sleep
 */

class MyThread extends Thread{
	public void run(){
		System.out.println(Thread.currentThread());
	}
}
class Demo{
	public static void main(String[] arg)throws InterruptedException{
		System.out.println(Thread.currentThread());       //Thread[threadName, priority, thread group]
                MyThread obj = new MyThread();
       	       obj.start();
	       Thread.sleep(2000);
	       Thread.currentThread().setName("ABC");
	       System.out.println(Thread.currentThread());
	}
}

