/* Concurrency methods in thread class :
 * Join
 */

class MyThread extends Thread{
	public void run(){
		for(int i=0; i<5; i++){
			System.out.println("Thread-0");
		}
	}
}
class ThreadDemo{
	public static void main(String[] arg)throws InterruptedException{
		MyThread obj = new MyThread();
		obj.start();
		obj.join();    // give all priority to that thread 
		for(int i=0; i<5; i++){
			System.out.println("In main thread");
		}
	}
}

