/* Deadlock scenario
 */

class thread extends Thread{
	static Thread nmMain = null;
	public void run(){
                try{
			nmMain.join();
		}catch(InterruptedException ie){
		}
		System.out.println("In thread-0");
	}
}
class demo{
	public static void main(String[] arg) throws InterruptedException{
	        thread.nmMain = Thread.currentThread();
		thread abc = new thread();
		abc.start();
		abc.join();
		System.out.println("In main thread");
	}
}
