/* Child thread group real time example
 */

class Aircraft extends Thread{
	Aircraft(ThreadGroup tg, String str){
		super(tg,str);
	}
	public void run(){
		System.out.println(Thread.currentThread());
	}
}
class India{
	public static void main(String[] arg){
		ThreadGroup India = new ThreadGroup("Aircraft");
		System.out.println();
		ThreadGroup Army = new ThreadGroup(India,"Army");
		Aircraft ac1 = new Aircraft(Army,"HAL Tejas");
		Aircraft ac2 = new Aircraft(Army,"Dassoult Rafale");
		Aircraft ac3 = new Aircraft(Army,"Sukhoi-30");
		ac1.setPriority(2);
		ac1.start();
		ac2.setPriority(2);
		ac2.start();
		ac3.setPriority(2);
		ac3.start();

		ThreadGroup Navy = new ThreadGroup(India,"Navy");
                Aircraft ac4 = new Aircraft(Navy,"F/A-18 Hornet");
                Aircraft ac5 = new Aircraft(Navy,"Boeing P8I");
                Aircraft ac6 = new Aircraft(Navy,"HAL Dhruv");
		ac4.setPriority(3);
		ac4.start();
                ac5.setPriority(3);
		ac5.start();
                ac6.setPriority(3);
	       	ac6.start();		
		
		 ThreadGroup Airforce = new ThreadGroup(India,"Airforce");
                Aircraft ac7 = new Aircraft(Airforce,"Sukhoi-30MKI");
                Aircraft ac8 = new Aircraft(Airforce,"Jaguar");
                Aircraft ac9 = new Aircraft(Airforce,"MiG-29");
                ac7.setPriority(1);
		ac7.start();
		ac8.setPriority(1);
                ac8.start();
		ac9.setPriority(1);
                ac9.start();
	}
}
