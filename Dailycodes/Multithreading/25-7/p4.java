/* Child thread group
 */

class MyThread extends Thread{
	MyThread(ThreadGroup tg, String str){
		super(tg,str);
	}
	public void run(){
		System.out.println(Thread.currentThread());
	}
}
class Demo{
	public static void main(String[] arg){
		ThreadGroup tGP = new ThreadGroup("C2W");
		MyThread obj1 = new MyThread(tGP,"C");
		MyThread obj2 = new MyThread(tGP,"Java");
		MyThread obj3 = new MyThread(tGP,"Python");
		obj1.start();
		obj2.start();
		obj3.start();
		ThreadGroup cGP = new ThreadGroup(tGP,"Incubator");
		MyThread obj4 = new MyThread(cGP,"Flutter");
		MyThread obj5 = new MyThread(cGP,"Springboot");
		obj4.start();
		obj5.start();

	}
}
