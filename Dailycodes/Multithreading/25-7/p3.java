/* Making thread group
 */

class MyThread extends Thread{
	MyThread(ThreadGroup tg, String str){
		super(tg, str);
	}
	public void run(){
		System.out.println(Thread.currentThread());
	}
}
class demoTG{
	public static void main(String[] arg){
		ThreadGroup tGP = new ThreadGroup("C2W");  //Passing group name
		MyThread obj1 = new MyThread(tGP,"C");   // Passing (Threadgroup name, thread name)
		MyThread obj2 = new MyThread(tGP,"Java");
		obj1.start();
		obj2.start();
	}
}



