/* Thread Group :
 */

class MyThread extends Thread{
	MyThread(String str){
		super(str);
	}
	public void run(){
		System.out.println(getName());
		System.out.println(Thread.currentThread().getThreadGroup());

	}
}
class Demo{
	public static void main(String[] arg){
		MyThread obj = new MyThread("ABC");    // passing thread name as parameter
		obj.start();
                MyThread obj1 = new MyThread("ABC");    // passing thread name as parameter
                obj1.start();
		System.out.println(Thread.currentThread().getThreadGroup());
	}
}
