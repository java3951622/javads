/* newSingleThreadExecutor()
 */

import java.util.concurrent.*;
class mythread implements Runnable{
	int num;
	mythread(int num){
		this.num = num;
	}
	public void run(){
		System.out.println(Thread.currentThread().getName()+"Start thread: "+num);
		System.out.println(Thread.currentThread().getName()+"End thread: "+num);
	}
}
class Demopool{
	public static void main(String[] arg){
		ExecutorService sr  = Executors.newSingleThreadExecutor();
		for(int i=1; i<=5; i++){
			mythread obj = new mythread(i);
			sr.execute(obj);
		}
	}
}
