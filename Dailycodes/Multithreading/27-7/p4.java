/* newFixedThreadPool(int)
 */

import java.util.concurrent.*;
class mythread implements Runnable{
	int num;
	mythread(int num){
		this.num = num;
	}
	public void run(){
		System.out.println(Thread.currentThread()+"Start thread:"+num);
		dailyTask();
                System.out.println(Thread.currentThread()+"End thread:"+num);
	}
       void dailyTask(){
		System.out.println("Running thread is :"+num);
	}
}
class poolDemo{
	public static void main(String[] arg){
		ExecutorService sr = Executors.newFixedThreadPool(5);
		for(int i=1; i<=10; i++){
			mythread obj = new mythread(i);
			sr.execute(obj);
		}
		sr.shutdownNow();
	}
}
