/* Active Count of threads & threadgroup
 */
/* Interrupt print, sleep interrupted when there is SOP in catch block
 */  
class mythread extends Thread{
	mythread(ThreadGroup tg, String str){
		super(tg,str);
	}
	public void run(){
		System.out.println(Thread.currentThread());
		try{
			Thread.sleep(2000);
		}catch(InterruptedException ie){
			System.out.println(ie.toString());
		}
	}
}
class Demo{
	public static void main(String[] arg){
		ThreadGroup tgp = new ThreadGroup("India");
		mythread t1 = new mythread(tgp,"Maharashtra");
		mythread t2 = new mythread(tgp,"Kashmir");
                t1.start();
		t2.start();

		ThreadGroup cgp1 = new ThreadGroup(tgp,"Pakistan");
		mythread t3 = new mythread(cgp1,"Karachi");
		mythread t4 = new mythread(cgp1,"Lahore");
                t3.start();
		t4.start();

		ThreadGroup cgp2 = new ThreadGroup(tgp,"Bangladesh");
	        mythread t5 = new mythread(cgp2,"Dhakka");
	        mythread t6 = new mythread(cgp2,"Mirpur");
	    	cgp2.interrupt();
		t5.start();
		t6.start();
cgp2.interrupt();

		System.out.println(tgp.activeCount());   //Count of active threads
		System.out.println(tgp.activeGroupCount());  //Count of active threadGroups in tgp
	        System.out.println(cgp1.activeCount());   //Count of active thread in cgp1
		System.out.println(cgp1.activeGroupCount()); //count of active threadGroups in cgp1
	}
}
