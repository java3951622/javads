/* Using runnable interface make threadgroup
 */

class mythread implements Runnable{
	public void run(){
		System.out.println(Thread.currentThread());
	}
}
class runnableDemo{
	public static void main(String[] arg){
		ThreadGroup tgp = new ThreadGroup("India");

		ThreadGroup cgp1 = new ThreadGroup(tgp,"Army");
		mythread obj1 = new mythread();
		mythread obj2 = new mythread();
		Thread t1 = new Thread(cgp1,obj1,"HAL Tejas"); //  (threadGroup name, runnable object, thread name)
		Thread t2 = new Thread(cgp1,obj2,"Sukhoi-30");
                t1.start();
		t2.start();

		ThreadGroup cgp2 = new ThreadGroup(tgp,"Navy");
		mythread obj3 = new mythread();
		mythread obj4 = new mythread();
		Thread t3 = new Thread(cgp2,obj3,"MiG 29K");
		Thread t4 = new Thread(cgp2,obj4,"F/A-18Hornet");
                t3.start();
		t4.start();
	}
}
