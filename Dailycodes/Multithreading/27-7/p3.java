/* ExecuteService newCachedThreadPool
 *
 */

import java.util.concurrent.*;
class Mythread implements Runnable{
	int num;
	Mythread(int num){
		this.num = num;
	}
	public void run(){
		System.out.println(Thread.currentThread()+" Start thread "+num);
		dailyTask(num);
		System.out.println(Thread.currentThread()+" End thread "+num);
	}
	void dailyTask(int i){
		try{
			Thread.sleep(3000);
		}catch(InterruptedException ie){
                      System.out.println("In running:"+i);
		}
	}
}
class threadpoolDemo{
	public static void main(String[] arg){
	  ExecutorService sr = Executors.newCachedThreadPool();
	  for(int i=1; i<=10; i++){
		  Mythread obj = new Mythread(i);
		  sr.execute(obj);
	  }
	  sr.shutdown();
	}
}

