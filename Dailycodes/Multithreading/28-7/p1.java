/* Use threadpool without ExecutorService 
 * typecast - ThreadPoolExecuter
 */
import java.util.concurrent.*;
class mythread implements Runnable{
	 int num;
	 mythread(int num){
		 this.num = num;
	 }
	 public void run(){
		 System.out.println("Start thread :"+num+ Thread.currentThread());
		 Task();
		 System.out.println("End thread :"+num+ Thread.currentThread());
	 }
	 void Task(){
		 try{
			 Thread.sleep(2000);
		 }catch(InterruptedException ie){
			 System.out.println("Thread running");
		 }
	 }
}
class Demo{
	public static void main(String[] arg){
		ThreadPoolExecutor te = (ThreadPoolExecutor)Executors.newFixedThreadPool(4);
		for(int i=1; i<=9; i++){
			mythread obj = new mythread(i);
			te.execute(obj);
		}
		te.shutdown();
	}
}


