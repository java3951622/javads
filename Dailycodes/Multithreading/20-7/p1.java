/* Overriding the start method
 * by overriding start meyhod new thread cannot be created
 */

class MyThread extends Thread{
	public void run(){
		System.out.println("In run");
		System.out.println("In run:"+Thread.currentThread().getName());
	}
	public void start(){
		System.out.println("In start");
		run();
	}
}
class threadDemo{
	public static void main(String[] arg){
		MyThread obj = new MyThread();
		obj.start();
		System.out.println("In main:"+Thread.currentThread().getName());
	}
}
