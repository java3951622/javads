/* Creating thread using Runnable interface
 */

class MyThread implements Runnable{
	public void run(){
		System.out.println("In run");
		System.out.println(Thread.currentThread().getName());
	}
}
class Threaddemo{
	public static void main(String[] ar){
		MyThread obj = new MyThread();
		Thread t = new Thread(obj);
		t.start();
		System.out.println(Thread.currentThread().getName());
	}
}
