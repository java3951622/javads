/* Set priority of thread
 */

class MyThread extends Thread{
	public void run(){
		Thread t = Thread.currentThread();
		System.out.println("Priority in Thread:"+t.getPriority());
	}
}
class Demo{
	public static void main(String[] arg){
         Thread t = Thread.currentThread();
	 System.out.println("Before set priority:"+t.getPriority());
	 MyThread obj1 = new MyThread();
	 obj1.start();
	 t.setPriority(7);
	  System.out.println("After set priority:"+t.getPriority());
	  MyThread obj2 = new MyThread();
	  obj2.start();
	}
}



