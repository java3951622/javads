/* Create thread in thread
 */

class MyThread2 extends Thread{
	public void run(){
		System.out.println("In thread2:"+Thread.currentThread().getName());
	}
}
class MyThread1 extends Thread{
	public void run(){
		System.out.println("In thread1:"+Thread.currentThread().getName());
		MyThread2 obj = new MyThread2();
		obj.start();
	}
}
class Demo{
	public static void main(String[] arg){
		System.out.println("In main:"+Thread.currentThread().getName());
		MyThread1 obj = new MyThread1();
		obj.start();
	}
}
