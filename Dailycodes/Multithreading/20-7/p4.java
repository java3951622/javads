/* Priority of thread: getPriority of thread
 */

class MyThread extends Thread{
	public void run(){
		Thread t = Thread.currentThread();
		System.out.println("In thread:"+t.getPriority());
	}
}
class ThreadDemo{
	public static void main(String[] arg){
		Thread t = Thread.currentThread();
		System.out.println("In main:"+t.getPriority());
		MyThread obj1 = new MyThread();
		obj1.start();
	}
}
