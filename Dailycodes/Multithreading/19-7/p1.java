/* Create thread using thread class
 */

class MyThread extends Thread{
	public void run(){
		for(int i=0; i<10; i++){
			System.out.println("In run");
		}
	}
}
class threadDemo{
	public static void main(String[] arg){
		MyThread obj = new MyThread();  //Born
		obj.start();     //Ready to run
	        for(int i=0; i<10; i++){
			System.out.println("In main");
		}
	}
}
