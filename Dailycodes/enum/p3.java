// @Override

class Parent{
	void m1(){
		System.out.println("In parent");
	}
}
class Child extends Parent{
	@Override
	void m1(int x){
		System.out.println("In child");
	}
}
class client{
	public static void main(String[] arg){
		Parent p = new Child();
		p.m1();
	}
}

