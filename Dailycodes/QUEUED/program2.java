/* Circular Queue
 */

import java.util.*;
class CircularQ{
	int queArr[];
	int front;
	int rear;
	int maxSize;
	CircularQ(int size){
		this.queArr = new int[size];
		this.front = -1;
		this.rear = -1;
		this.maxSize = size;
	}
	void enqueue(int data){
		if((front == 0 && rear == maxSize-1) || ((rear+1)%maxSize == front)){
			System.out.println("Q is full");
			return;
		}else if(front == -1){
			front = rear = 0;
		}else if(rear == maxSize-1 && front != 0){
			rear = 0;
		}else{
			rear++;
		}
		queArr[rear] = data;
	}
	int dequeue(){
		 if(front == -1){
			 System.out.println("Q is empty");
			 return -1;
		 }else{
			 int ret = queArr[front];
			 if(front == rear){
				 rear = front = -1;
		         }else if(front == maxSize-1){
			 front = 0;
		         }else{
			 front++;
		         }
		 return ret;
		 }
	}
	void printQ(){
		if(front <= rear){
			for(int i=front; i<=rear; i++){
				System.out.print(queArr[i]+" ");
			}
		}else{
			for(int i=front; i<maxSize; i++){
				System.out.print(queArr[i]+" ");
			}
			for(int i=0; i<=rear; i++){
				System.out.print(queArr[i]+" ");
			}
		}
		System.out.println();
	}

}


class Client{
	public static void main(String[] arg){
		System.out.println("Enter size of array:");
		Scanner sc = new Scanner(System.in);
		int size = sc.nextInt();
		CircularQ cq = new CircularQ(size);

		char ch;
		do{
			System.out.println("1.Enqueue");
			System.out.println("2.Dequeue");
			System.out.println("3.Print queue");

			System.out.println("Enter choice:");
			int choice = sc.nextInt();
			switch(choice){
				case 1:{
					       System.out.println("Enter data:");
					       int data = sc.nextInt();
					       cq.enqueue(data);
				       }
                                       break;
				case 2:{
					       int ret  = cq.dequeue();
					       if(ret != -1 )
						       System.out.println(ret +"is popped");
	         		 	}
				       break;
			       case 3:{
					      cq.printQ();
			              }
				      break;
			       default:
				      System.out.println("Wrong choice");
				      break;
			}
			System.out.println("Do u want continue:");
			ch = sc.next().charAt(0);
		}while(ch == 'y' || ch == 'Y');
	}
}

