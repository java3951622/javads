import java.util.*;

class Queue{
	int queArr[];
	int front;
	int rear;
	int maxSize;
	Queue(int size){
		this.queArr = new int[size];
		this.front = -1;
		this.rear  = -1;
		this.maxSize = size;
	}
	void enqueue(int data){
		if(rear == maxSize-1){
			System.out.println("Queue is full");
			return;
		}
		if(front == -1 && rear ==-1){
			front = rear = 0;
			queArr[rear] = data;
		}else{
			rear++;
			queArr[rear] = data;
		}

	}
	int dequeue(){
		if(front == -1){
			System.out.println("Queue is empty");
			return -1;
		}else{
                        int ret = queArr[front];
			front++;
			if(front > rear )
				front = rear = -1;
			return ret;
		}

	}
	boolean empty(){
		if(front == -1)
			return true;
		else
		        return false;
	}
	int front(){
		if(front == -1){
			System.out.println("Queue is empty");
			return -1;
		}else{
			return queArr[front];
        	}
	}
	int rear(){
		if(rear == -1){
		       System.out.println("Queue is empty");
		       return -1;
		}else{
			return queArr[rear];
	        }
        }
	void printQueue(){
	        if(front == -1){
			System.out.println("Queue is empty");
			return;
		}else{
			for(int i=front; i<=rear; i++){
				System.out.print(queArr[i]+"\t");
			}
			System.out.println();
		}

	}

}

class Client{
	public static void main(String[] arg){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size:");
		int s = sc.nextInt();
		Queue Q = new Queue(s);
		char ch;
		do{
			System.out.println("1.Enqueue");
			System.out.println("2.dequeue");
			System.out.println("3.empty");
			System.out.println("4.front");
			System.out.println("5.rear");
			System.out.println("6.Print queue");

			System.out.println("Enter ur choice:");
			int choice = sc.nextInt();
			switch(choice){
				case 1:
					{
						System.out.println("Enter data to enqueue:");
						int data = sc.nextInt();
						Q.enqueue(data);
					}
					break;
				case 2:
					{
						int ret = Q.dequeue();
						if(ret != -1)
							System.out.println(ret + "popped");
					}
					break;
				case 3:
					{
						boolean ret = Q.empty();
						if(ret)
							System.out.println("Q is empty:");
						else
							System.out.println("Q is not empty");
					}
					break;
				case 4:
					{
						int ret = Q.front();
						if(ret != -1)
							System.out.println("Front is:"+ret);
					}
					break;
				 case 5:
                                        {
                                                int ret = Q.rear();
                                                if(ret != -1)
                                                        System.out.println("Rear is:"+ret);
                                        }
                                        break;
				case 6:
					{
						Q.printQueue();
					}
					break;
				default:
					System.out.println("Wrong choice");
					break;
			}
			System.out.println("Do u want continue:");
			ch = sc.next().charAt(0);
		}while(ch == 'y' || ch == 'Y');
	}
}
