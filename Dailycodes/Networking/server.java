

import java.net.*;
import java.io.*;
class Server{
	public static void main(String[] arg)throws IOException{
		ServerSocket ss = new ServerSocket(200);
		Socket s = ss.accept();
		System.out.println("Connection established");
		OutputStream os = s.getOutputStream();
		PrintStream ps = new PrintStream(os);
		ps.println("Hello Client");
		ps.println("OK! Bye");
		ps.close();
		s.close();
		ss.close();
	}
}
