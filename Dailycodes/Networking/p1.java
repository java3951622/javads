/*  
 */

import java.io.*;
import java.net.*;
class IPaddress{
	public static void main(String[] arg)throws UnknownHostException,IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the website:");
		String site = br.readLine();
		InetAddress ip = InetAddress.getByName(site);
		System.out.println("IP address = "+ip);
	}
}
