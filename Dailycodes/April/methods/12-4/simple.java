/* Call non-static method from static method by obj
 */

class demo{
	int x=20;
	static int y=50;
	static void fun(){
		demo obj = new demo();
		System.out.println(obj.x);
		System.out.println(y);
	}
	void gun(){
		System.out.println(x);
		System.out.println(y);
	}
	public static void main(String[] arg){
		
		demo obj=new demo();
		fun();
		obj.gun();
		 System.out.println(obj.x);
                System.out.println(y);
	}
}

