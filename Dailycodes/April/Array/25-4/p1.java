/*Passing array to method
 */

class demo{
	static void fun(int arr[]){
		System.out.println("In fun method:");
		for(int x: arr){
			System.out.println(x);
		}
	}
	public static void main(String[] arg){
		int arr[] = {10,20,30,40};
		for(int a : arr){
			System.out.println(a);
		}
		fun(arr);
	}
}
