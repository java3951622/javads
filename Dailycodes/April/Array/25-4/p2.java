/*Passing array to method and make changes in the array.
 */

class Demo{
	static void fun(int arr[]){
		for(int x : arr){
			System.out.println(x);
		}
		for(int i=0; i<arr.length; i++){
			arr[i] = arr[i]+50;
		}
	}
	public static void main(String[] arg){
		int arr [] = {20,30,40,50,60};
		fun(arr);
		System.out.println("After making changes in :");
		for(int x : arr){
			System.out.println(x);
		}
	}
}

