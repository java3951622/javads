/* Print the count of odd & even numbers of array.
 */

import java.io.*;
class num{
	public static void main(String[] arg)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size:");
		int size = Integer.parseInt(br.readLine());
		int[] arr = new int[size];
		int c1=0,c2=0;
		System.out.println("Enter the elements:");
		for(int i=0; i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
			if(arr[i]%2 == 0){
				c1++;
			}else{
				c2++;
			}
		}
		System.out.println("Even count = "+c1);
		System.out.println("Odd count = "+c2);
	}
}

