/* Sum of elements of array
 */
 import java.io.*;
 class sum{
	 public static void main(String[] arg)throws IOException{
		 BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		 System.out.println("Enter the size of array:");
		 int size = Integer.parseInt(br.readLine());
		 int arr[] = new int [size];
		 int sum=0;
		 System.out.println("Enter the elements of array:");
		 for(int i=0; i<arr.length; i++){
			 arr[i] = Integer.parseInt(br.readLine());
			 sum = sum+arr[i];
		 }
		 System.out.println("Sum of elements = "+sum);
	 }
 }



