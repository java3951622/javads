

class Demo{
	void fun(int arr[]){
		arr[1] = 70;
		arr[2] = 80;
	}
	public static void main(String[] arg){
		int arr[] = {10,20,30,40};
		System.out.print("Hash code:");
		/*for(int i=0; i<arr.length; i++){
		 System.out.println(System.identityHashCode(arr[i]));
		}*/
		System.out.println(System.identityHashCode(arr[0]));
		System.out.println(System.identityHashCode(arr[1]));
		System.out.println(System.identityHashCode(arr[2]));
		Demo obj = new Demo();
		obj.fun(arr);
		System.out.print("After changes in array:");
		for(int a : arr){
			System.out.println(a);
		}
		int x = 70;
		int y = 80;
	
		System.out.println("X & Y hash codes:");
		System.out.println(System.identityHashCode(x));
		System.out.println(System.identityHashCode(y));
	}
}
 
