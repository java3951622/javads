/* Basic @D array
 */

import java.io.*;
class demo{
	public static void main(String[] arg)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the count of rows:");
		int r = Integer.parseInt(br.readLine());
		System.out.println("Enter the count of column:");
                int c = Integer.parseInt(br.readLine());
		int [] arr [] = new int[r][c];
		System.out.println("ENter the array elements:");
		for(int i=0; i<arr.length; i++){
			for(int j=0; j<arr[i].length; j++){
				arr[i][j] = Integer.parseInt(br.readLine());
			}
		}
		System.out.println("Array elements are:");
		for(int i=0; i<arr.length; i++){
			for(int j=0; j<arr[i].length; j++){
				System.out.print(arr[i][j]+"\t");
			}
			System.out.println();
		}
	}
}

