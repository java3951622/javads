/* Jagged array 
 */

class demo{
	public static void main(String[] arg){
		int arr[][] = {{1},{4,5},{7,8,9}};
		for(int i=0; i<arr.length; i++){
			for(int j=0; j<arr[i].length; j++){
				System.out.print(arr[i][j]+"\t");
			}
			System.out.println();
		}
	}
}
