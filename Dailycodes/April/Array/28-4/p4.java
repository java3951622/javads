/* Jagged array initialize method 2.Take all inputs from user.
 */

import java.io.*;
class  demo{
	public static void main(String[] arg)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	        System.out.print("Enter the row count:");
	        int row = Integer.parseInt(br.readLine());
       
		int arr[][] =  new int [row][];
		System.out.println("Enter the count of columns in each row:");
		for(int i=0; i<row; i++){

		arr[i] = new int[Integer.parseInt(br.readLine())];
		}
		System.out.println("ENter the array elements:");
		for(int i=0; i<arr.length; i++){
			for(int j=0; j<arr[i].length; j++){
				arr[i][j] = Integer.parseInt(br.readLine());
			}
		}
		System.out.println("Array elements are:");
		for(int [] x : arr){
			for(int y : x){
				System.out.print(y+"\t");
			}
			System.out.println();
		}
	}
}



