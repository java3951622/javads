/* Jagged array initialize method 2.
 */

import java.io.*;
class  demo{
	public static void main(String[] arg)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	        	
		int arr[][] =  new int [3][];
		arr[0] = new int[3];
		arr[1] = new int[1];
		arr[2] = new int[3];
		System.out.println("ENter the array elements:");
		for(int i=0; i<arr.length; i++){
			for(int j=0; j<arr[i].length; j++){
				arr[i][j] = Integer.parseInt(br.readLine());
			}
		}
		System.out.println("Array elements are:");
		for(int [] x : arr){
			for(int y : x){
				System.out.print(y+"\t");
			}
			System.out.println();
		}
	}
}



