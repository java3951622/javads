/* Access 2D array using for-each loop
 */

class Demo{
	public static void main(String[] arg){
		int arr[][] = {{1,2,3,4},{5,6,7,8},{9,4,3}};
		for(int[] x : arr){
			for(int y : x){
				System.out.print(y+"\t");
			}
			System.out.println();
		}
	}
}
