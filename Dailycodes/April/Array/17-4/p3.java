// Array using Float.

import java.io.*;
class array{
	public static void main(String[] arg)throws IOException{
	       BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
             System.out.println("Enter the size:");
              int s = Integer.parseInt(br.readLine());
	      float  arr[] = new float[s];
	      System.out.println("Enter the elements:");
	      for(int i=0; i<s; i++){
		      arr[i] = Float.parseFloat(br.readLine());
	      }
	      System.out.println("Array elements");
	      for(int i=0; i<s; i++){
		      System.out.print(arr[i]+"\t");
	      }
	}
}
