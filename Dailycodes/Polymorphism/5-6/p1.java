//Overrider

class Parent{
	Parent(){
		System.out.println("In Parent Constructor");
	}
	void Property(){
		System.out.println("Home,Car");
	}
	void marry(){
		System.out.println("ABC");
	}
}
class Child extends Parent{
	Child(){
		System.out.println("In constructor");
	}
	void marry(){
		System.out.println("XYZ");
	}
}
class Client{
	public static void main(String[] arg){
		Child obj = new Child();
		obj.Property();
		obj.marry();
	}
}

