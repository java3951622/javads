

class Parent{
	Parent(){
		System.out.println("In Parent constructor");
	}
	void fun(){
		System.out.println("In parent fun");
	}
}
class Child extends Parent{
	Child(){
		System.out.println("In Child constructor");
	}
	void gun(){
		System.out.println("In Child gun");
	}
}
class main{
	public static void main(String[] arg){
		Parent obj1 = new Parent();
	         obj1.fun();
//	         obj1.gun();

	         Child obj2 = new Child();
	          obj2.fun();
	          obj2.gun();
	}
}	
