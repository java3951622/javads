/* Basic Polymorphism code of Overloader
 * Overloader- many methods of same name with different parameters in same class
 */

class Demo{
	void fun(int x){
		System.out.println(x);
	}
	void fun(float x){
		System.out.println(x);
	}
	void fun(Object obj){   //fun(Demo obj)
		System.out.println("In demo para");
		System.out.println(obj);
	}
	public static void main(String[] arg){
		Demo obj = new Demo();
		obj.fun(20);
		obj.fun(30.7f);
		Demo obj1 = new Demo();
		obj1.fun(obj);
		System.out.println(obj1);
	}
}
