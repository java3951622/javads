/* Same Static variables access in Inheritance
 */

class Parent{
	int x = 10;
	static int y = 20;
	Parent(){
		//System.out.println(super.x);
		System.out.println("Parent");
	}
	void fun(){
		System.out.println(x);
	}
}
class Child extends Parent{
	int x = 100;
	static int y = 200;
	Child(){
		 super(fun());
		System.out.println("Child");
	}
void access(){
	//super(fun());
	//System.out.println(super(y));
	System.out.println(super.x);
	 System.out.println(super.y);
		
	   System.out.println(y);
}
}
class Client{
	public static void main(String[] arg){
		Child obj = new Child();
		obj.access();
	}
}


