  //Ststic modifier

class Parent{
	static void fun(){
		System.out.println("In parent fun");
	}
}
class Child extends Parent{
	static void fun(){
		System.out.println("In child fun");
	}
}
class Client{
	public static void main(String[] arg){
		Parent obj = new Parent();
		obj.fun();
		Child obj1 = new Child();
		obj1.fun();
		Parent obj2 = new Child();
		obj2.fun();
	}
}
