 
class Parent{
	void fun(){   // access modifier - default
		System.out.println("In parent fun");
	}
}
class Child extends Parent{
	public void fun(){       //access modifier - public
		System.out.println("In child fun");
	}
}
class Client{
	public static void main(String[] arg){

	Parent obj = new Child();
	obj.fun();
	}
}

