class Demo{
	void fun(String str1){
		System.out.println("String");
	}
	void fun(StringBuffer str2){
		System.out.println("StringBuffer");
	}
}
class client{
	public static void main(String[] arg){
		Demo obj = new Demo();
		obj.fun("core2web");
		obj.fun(new StringBuffer("core2web"));
		//obj.fun(null);        ambiguity error
	}
}
