/* Real time example for overriding
 */

class Match{
	void matchType(){
		System.out.println("T20/ODI/Test");
	}
}
class IPLMatch extends Match{
	void matchType(){
		System.out.println("T20");
	}
}
class TestMatch extends Match{
	void matchType(){
		System.out.println("Test");
	}
}
class viewer{
	public static void main(String[] arg){
		Match type1 = new IPLMatch();
		type1.matchType();
		Match type2 = new TestMatch();
                type2.matchType();
	}
}
