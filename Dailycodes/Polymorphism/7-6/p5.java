class Parent{
	Object fun(){
		System.out.println("object ");
		return new Object();
	}
}
class Child extends Parent{
	String fun(){
		System.out.println("String");
		return "string";
	}
}
class client{
	public static void main(String[] arg){
		Parent obj = new Child();
		obj.fun();
	}
}

