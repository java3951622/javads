class Demo{
	void fun(Object obj){
		System.out.println("Object");
	}
	void fun(String str){
		System.out.println("String");
	}
}
class client{
	public static void main(String[] arg){
		Demo obj = new Demo();
		obj.fun("core2web");
		obj.fun(new StringBuffer("core2web"));
		obj.fun(null);//priority gives to child object is parent & string is null
	}
}
